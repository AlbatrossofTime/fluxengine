#pragma once
#include <GameObjectManager.h>
#include "Engine.h"

extern FluxEngine* ENGINE;

class Level
{
public:
  Level(std::string &name);
  ~Level();

  GameObject* GetLevel();

private:
  GameObject *level_;
  void CreateLevel(std::string &name);
};

