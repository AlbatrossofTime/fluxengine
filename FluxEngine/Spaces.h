#pragma once
#include <string>
#include "level.h"

class Spaces
{
public:
  Spaces(std::string &name);
  
  void Init() {}
  void Update(){}
  void Shutdown(){}


  /*get space name*/
  std::string  GetName();

  /*add and remove systems*/
  void addSystem(Systems::System*& sys);
  void removeSystem(Systems::System*& sys);

  /*camera init*/
  void CreateCamera();
  void GetCamera();

  /*add objects to space*/
  void addGameObject(GameObject*& game_object);
  void removeGameObject(GameObject*& game_object);

  void cleanSpace();//remove everything from the space

 ~Spaces();
private:
  //spaces need to be constructed by name
  Spaces() = delete;
  Spaces(Spaces &space) = delete;

  Level level_;
  //going to build custom containter, dont like stacks too small and very limited with no debug.
  std::vector<GameObject*> objects_;
  std::vector<Systems::System*> systems_;
  /*containter of all components in space --will add*/
  //std::vector<Transform> tranform_component_vector_;
  //std::vector<Physics> physics_component_vector_;
  //std::vector<SquareMesh> squaremesh_component_vector_;
};