/**************************************************************************/
/*!
\file   GameObjectFactory.h
\author Josh Ibrahim
\par
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief
responsible for creating and destroying game objects
Functions include:

*/
/**************************************************************************/

#pragma once
#include "stdafx.h"
#include "System.h"
#include  <vector> //replace with boost vec
#include  "GameObject.h"
//#include "Transform.h"

//class GameObject;
class GameObjectFactory
{
public:
  GameObjectFactory(); //needed to declare system
  ~GameObjectFactory();

  /*create and destroy objects*/
  void Create(std::string name);
  void Delete(GameObject * pObj);

  std::vector<GameObject*> OM; // vector to manage objects
protected:
  unsigned int GameObjectTotal;
private:
  unsigned int LastID;
};