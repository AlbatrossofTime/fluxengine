/**************************************************************************/
/*!
\file   Engine.h
\author 
\par
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief
responsible for creating and destroying game objects
Functions include:

*/
/**************************************************************************/
#pragma once
#include "stdafx.h"

#ifndef _ENGINE_H
#define _ENGINE_H


// Includes
#include "GameObjectFactory.h"
#include <vector>
#include <stack>
//#include "Time.h"
#include "System.h"
#include "Vector2.h" // SLR: temp included for temp storage state objects
#include "SeanSpaces.h"

//c runtime header files

#ifndef _WINDOWS_
  #include <Windows.h>
#endif // !_WINDOWS_

#ifndef _MAP_
  #include <map>
#endif // !_MAP_

//Enum for engine state
enum EngineState
{
  Invalid,
  Constructing,
  Init,
  Running,
  ShuttingDown,
  Destroying,
};


// SLR: Completely temporary until we have game objects
struct tempStateHolder {
	bool stateUp;
	bool stateDown;
	bool stateLeft;
	bool stateRight;
	float width;
	float height;
	unsigned int indicesP[6] = {  // note that we start from 0!
		0, 1, 2,  // first Triangle
		1, 2, 3   // second Triangle
	};
	Vec2<float,float> position;
	std::vector<Vec2<float,float>> mesh;
  std::vector<Vec2<float, float>> openGLContainer; // SLR : vert & tex coords, hopefully
	char* sprite;
};

class FluxEngine
{
public:
  void InitPlayerObj(); // SLR - temp
  FluxEngine();
  ~FluxEngine();
  void GameLoop();
  void Initialize();
  void ShutDown();
  void Stop();
  
  static EngineState GetEngineState() { return m_EngineState; }

  // SLR Temporary Presentation Objects
  tempStateHolder *playerObject;
  void calcMesh();

  template<typename T>
  std::shared_ptr<T> GetSystem(Systems::SystemType sysType);

  // Factory from which client will request to create game objects.
  GameObjectFactory* FACTORY;

private:
  //Current Engine State
  static EngineState m_EngineState;

  // Time since last frame.
  float dt;

  // Bucket of shared ptr's to Systems, to iterate through during the GameLoop.
  std::vector<Systems::SystemPtr>Systems;

  // Stack of Spaces - only update systems registered to top of stack?
  std::stack<SeanSpace*> FluxSpaces;
};


// SLR - As per AJEngine, this is the primary entry point into the systems vector.
// Ex. of syntax: 
//   WindowPtr wat = GETSYS(Window);
#define GETSYS( systype ) \
ENGINE->GetSystem<systype>(Systems::Sys_##systype)

template <typename T>
std::shared_ptr<T> FluxEngine::GetSystem(Systems::SystemType sysType)
{
	for (auto &it : Systems)
	{
		if (it->_type == sysType)
			return std::static_pointer_cast<T>(it);
	}

	// Throw an error if the system doesn't exist in the engine.
	throw std::range_error("The specified system does not exist.");
}

#endif // !_ENGINE_H