#pragma once
#include "stdafx.h"
#include "System.h"
#include "GameObject.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include "Engine.h"

extern FluxEngine* ENGINE;
/**
 * \brief
 * The exception class for JCS
 */
class JCSException : public std::exception
{
public:
  
  JCSException(std::string const& error_msg);
  
  virtual ~JCSException(){}
  
  virtual const char* what();
  
private:
  std::string reason_;
};

class JCS : public Systems::System
{
public:
  JCS(const Systems::SystemType type = Systems::Sys_JCS, std::string  extension = ".txt"); //needed to declare system
  ~JCS();

  
  void SaveObjtxt(GameObject*& v);

  
  GameObject* LoadObjtxt(std::string filename);

private:
 std::string ext_; //can be .json doesnt matter moving to binary

  //helper for writing  components in text
 void WritePhysics(const std::ofstream& outfile, GameObject* game_object);
 void WriteTransform(std::ofstream& outfile, GameObject*& o);
 void WritePhysics(std::ofstream& outfile, GameObject*& o);
 void WriteCollider(std::ofstream& outfile, GameObject*& o);
  //helpers for reading components in text
 void ReadTransform(std::vector<std::string> input, GameObject*& o);
 void ReadPhysics(std::vector<std::string> input, GameObject*& o);
 void ReadCollider(std::vector<std::string> input, GameObject*& o);

  //JFI::TODO vectors for levels and gameobjects
};

