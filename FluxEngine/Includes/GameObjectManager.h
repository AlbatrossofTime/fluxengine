/**************************************************************************/
/*!
\file   GameObjectManager.h
\author Josh Ibrahim
\par
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief
  responsible for creating and destroying game objects
Functions include:

*/
/**************************************************************************/
#pragma once
#include "stdafx.h"
#include "System.h"
#include  <vector> //replace with boost vec
#include  "GameObject.h"
//#include "Transform.h"

class GameObject;
class GameObjectManager : public Systems::System
{
public:
  GameObjectManager(const Systems::SystemType type = Systems::Sys_GameObjectManager); //needed to declare system
  ~GameObjectManager();

  /*inheritied from system*/
  void Init();
  void ShutDown();
  virtual void Update(float);
  
  
  
  /*create and destroy objects*/
  void Create(GameObject*& pObj, std::string name);
  void Delete(GameObject*& pObj);

  std::vector<GameObject*> OM; // vector to manage objects
protected:
  unsigned int GameObjectTotal;
private:
  unsigned int LASTID;
};

