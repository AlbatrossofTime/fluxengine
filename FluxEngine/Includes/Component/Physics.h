#include "component.h"
#include "Vector2.h"
#include "Collider.h"
#include "Transform.h"

class Physics : public Component
{
private:
  float velocity;
  float acceleration;
  float orientation;
  Collider CollisionBox;

public:
  Physics() : Component(C_TYPE::C_Physics), acceleration(0.0f), velocity(1.0f), orientation(0.0f), CollisionBox(Collider::Collider())
  {
    //SGS: Default constructor for transform class.
  }

  ~Physics() {}
  //virtual void Update() override;



  //SGS: Getters
  float GetVelocity();
  float GetAcceleration();
  float GetOrientation();
  Collider GetCollider();

  //SGS: Setters
  void SetVelocity(float velocity);
  void SetAcceleration(float acceleration);
  void SetOrientation(float orientation);
  void SetCollider(Collider collisionbox);
};
