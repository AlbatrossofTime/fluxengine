#pragma once
#include "stdafx.h"
/*MADE BY SS*/
#include "Vector2.h"
#include "Matrix3.h"
//#include "glm.hpp"
#include <memory>
#include "component.h"
/**************************************************************************/
/*!
\file   Transform.h
\author
\par
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief

Functions include:

*/
/**************************************************************************/

typedef unsigned int uint;

class Transform : public Component
{
private:

  Vec2<> scale;    //SGS: Object's size
  float rotation;     //SGS: Object's Rotation
  bool isDirty;       //SGS: If true, matrix must be recalculated
  Matrix3 CurrentTransform; //SGS: The 3x3 matrix of the object

  uint id;
public:
  

  Transform() : Component(C_TYPE::C_Transform), position({ 0.0f, 0.0f }), scale({ 1.0f, 1.0f }), rotation(0.0f), isDirty(true), id(0)
  {
    //SGS: Default constructor for transform class.
  }

  void TransformUpdate(uint id);
  void TranslateByVec(Vec2<> translationvector);

  Vec2<> position; //SGS: Object's translation/position in world space

            //SGS: Getter Functions for transform components
  Vec2<> GetPosition();
  Vec2<> GetScale(uint id);
  float GetRotation(uint id);
  bool GetisDirty(uint id);

  Matrix3 GetMatrix(uint id)
  {
    return CurrentTransform;
  };


  //SGS: Setter Functions for transform component
  void SetPosition(uint id, Vec2<> position);
  void SetScale(uint id, Vec2<> scale);
  void SetRotation(uint id, float rotation);
  void SetisDirty(uint id, bool is_it_dirty);
  //virtual void Update() override;
 ~Transform(){}

};
