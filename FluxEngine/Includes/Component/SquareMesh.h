#pragma once
#include "component.h"
#include "Vector2.h"
#include <vector>
#include "Collider.h"

class SquareMesh : public Component
{
private:
	std::vector<Vec2<>> Vertices;

public:
	SquareMesh() : Component(C_TYPE::C_SquareMesh)
	{
		//SGS: This defaults the mesh to a unit square centered at 0,0
		Vertices.push_back(Vec2<>(1.0f, 1.0f));
		Vertices.push_back(Vec2<>(-1.0f, 1.0f));
		Vertices.push_back(Vec2<>(1.0f, -1.0f));
		Vertices.push_back(Vec2<>(-1.0f, -1.0f));
	}

	~SquareMesh() {}


	//SGS: Sets the vertices in the vector to the specified values
	void Set(Vec2<> TopR, Vec2<> TopL, Vec2<> BotR, Vec2<> BotL);
	//SGS: Returns the vector of vertices
	std::vector<Vec2<>> Get(void);

	
};
