/**************************************************************************/
/*!
\file   component.h
\author Josh Ibrahim
\par
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief
responsible for creating and destroying game objects
Functions include:

*/
/**************************************************************************/
#pragma once
#include "stdafx.h"
#include "GameObjectUmbrella.h"
  /*responsible for maintaining and deleting components*/
class GameObject;
class Component
{
public:
  enum C_TYPE : signed int { C_Transform = 0, C_Physics, C_Sprite, C_SquareMesh, C_Max_Num };
  Component(const C_TYPE type);

  // virtual void Update() = 0;

  virtual ~Component();

  /*ptr to assign owner of component*/
  GameObject* mOwner;

  /*store C_type*/
  C_TYPE mType;
};
