// SLR - temp SeanSpaces class header

#include "GameObject.h"
#include <vector>
#include <stack>

#pragma once

enum SpaceType
{
  Space_MainMenu,
  Space_MAX
};

class SeanSpace {
  friend class FluxEngine;
  friend class std::stack<SeanSpace>;
public:
    SeanSpace(std::string &name, SpaceType type):
      _name(name), _type(type) {ComposeSpace(_type);};
    ~SeanSpace();

    void ComposeSpace(SpaceType type);
    void addObject(GameObject* id);
    void removeObject(GameObject* id);

private:
  // SeanLevel currentLevel; // Future potentiality
  SeanSpace() = delete; // SLR - no default spaces
  std::string _name;
  SpaceType   _type;
  std::vector<GameObject*> spaceObjects;
};