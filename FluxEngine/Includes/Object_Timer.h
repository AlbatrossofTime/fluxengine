/******************************************************************************/
/*!
\file   Object_Timer.h
\author Deni Van Winkle
\par    email: deni.vanwinkle\@digipen.edu
\par    DigiPen login: deni.vanwinkle
\par    Course: GAM200
\par    Project: Shisan

Copyright � 2017 DigiPen (USA) Corporation.
*/
/******************************************************************************/
#include <chrono>

using namespace std;

class OTimer
{
public:
	OTimer(bool Minutes, float timeLimit);

	void Start();
	void Pause();
	bool Update();
	float GetTimeLimit();
	float GetTimeLeft();


private:
	chrono::steady_clock::time_point TimerStart;
	chrono::steady_clock::time_point LastTimePoint;
	chrono::steady_clock::time_point CurrentTimePoint;

	float TimeLimit;
	bool Minutes;
	float TimeLeft;
	bool On;
};
