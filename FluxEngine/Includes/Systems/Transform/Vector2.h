#pragma once
#include <cmath>

template<typename U = float, typename V = float>
struct Vec2 {
public:
	U x;
	V y;

	// Default constructor initializes to zero
	Vec2();
	~Vec2();

	Vec2(U _x, V _y);
	Vec2(const Vec2 &v);
	Vec2(const Vec2 *v);

	void Set(U x, V y);
	
	float Length() const;
	float LengthSquared() const;
	float Distance(const Vec2 &v) const;
	float DistanceSquared(const Vec2 &v) const;
	float Dot(const Vec2 &v) const;
	//float Cross(const Vec2 &v) const;

	Vec2 Normal();
	void Normalize();

	Vec2& operator=(const Vec2 &v);
	Vec2& operator=(const float &f);
	Vec2& operator-();

	bool operator==(const Vec2 &v) const;
	bool operator!=(const Vec2 &v) const;

	Vec2 operator+(const Vec2 &v) const;
	Vec2 operator-(const Vec2 &v) const;
	Vec2 operator*(const Vec2 &v) const;
	Vec2 operator/(const Vec2 &v) const;

	Vec2& operator+=(const Vec2 &v);
	Vec2& operator-=(const Vec2 &v);
	Vec2& operator*=(const Vec2 &v);
	Vec2& operator/=(const Vec2 &v);

	Vec2 operator+(float &f);
	Vec2 operator-(float &f);
	Vec2 operator*(float &f);
	Vec2 operator/(float &f);

	Vec2& operator+=(float &f);
	Vec2& operator-=(float &f);
	Vec2& operator*=(float &f);
	Vec2& operator/=(float &f);

	// SLR TODO: how to handle this with template parmaters??
	float& operator[](int i);
	float operator[](int i) const;

};