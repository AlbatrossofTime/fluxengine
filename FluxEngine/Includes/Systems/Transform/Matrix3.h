#pragma once
#include <cmath>
#include "Vector3.h"
#include "Vector2.h"

struct Matrix3 {
	Vec3<> row[3];
	Vec3<>& operator[](int i);
	const Vec3<>& operator[](int i) const;

	Matrix3();
	Matrix3(const Vec3<> &Lx, const Vec3<> &Ly, const Vec3<> &W);

	// 2D Operations
	float determinant(float a, float b, float c, float d);
	Matrix3 transpose(Matrix3 A);
	// 2D Transformations
	//Matrix3 Rot(float t, const Vec2<> &v);
	Matrix3 Rot(float t);
	Matrix3 Trans(const Vec2<> &v);
	Matrix3 Scale(float r);
	Matrix3 Scale(float rx, float ry);
	//Matrix3 Inverse(const Matrix3& A);

  // 3x3 Matrix by Matrix operations
  Matrix3& operator*=(const Matrix3& A);
};

Vec3<> operator*(const Matrix3& A, const Vec3<>& v);
Matrix3 operator*(const Matrix3& A, const Matrix3& B);