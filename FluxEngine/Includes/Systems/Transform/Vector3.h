#pragma once
#include <cmath>

template<typename U = float, typename V = float, typename W = float>
struct Vec3 {
public:
	U x;
	V y;
	W z;

	// Default constructor initializes to zero
	Vec3();
	~Vec3();

	Vec3(U _x, V _y, W _z);
	Vec3(const Vec3 &v);
	Vec3(const Vec3 *v);

	void Set(U x, V y, W z);

	float Length() const;
	float LengthSquared() const;
	float Distance(const Vec3 &v) const;
	float DistanceSquared(const Vec3 &v) const;
	float Dot(const Vec3 &v) const;
	Vec3 Cross(const Vec3 &v) const;

	Vec3 Normal();
	void Normalize();

	Vec3& operator=(const Vec3 &v);
	Vec3& operator=(const float &f);
	Vec3& operator-();

	bool operator==(const Vec3 &v) const;
	bool operator!=(const Vec3 &v) const;

	Vec3 operator+(const Vec3 &v) const;
	Vec3 operator-(const Vec3 &v) const;
	Vec3 operator*(const Vec3 &v) const;
	Vec3 operator/(const Vec3 &v) const;

	Vec3& operator+=(const Vec3 &v);
	Vec3& operator-=(const Vec3 &v);
	Vec3& operator*=(const Vec3 &v);
	Vec3& operator/=(const Vec3 &v);

	Vec3 operator+(float &f);
	Vec3 operator-(float &f);
	Vec3 operator*(float &f);
	Vec3 operator/(float &f);

	Vec3& operator+=(float &f);
	Vec3& operator-=(float &f);
	Vec3& operator*=(float &f);
	Vec3& operator/=(float &f);

	float& operator[](int i);
	float operator[](int i) const;
};