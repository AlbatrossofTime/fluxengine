/******************************************************************************/
/*!
\file   Audio_Engine.h
\author Deni Van Winkle
\par    email: deni.vanwinkle\@digipen.edu
\par    DigiPen login: deni.vanwinkle
\par    Course: GAM200
\par    Project: Shisan

Copyright � 2017 DigiPen (USA) Corporation.
*/
/******************************************************************************/
#ifndef Audio_Engine
#define Audio_Engine
#include "fmod_studio.hpp"
#include "fmod.hpp"
#include "System.h"
#include <string>
#include <map>
#include <iostream>

using namespace std;

typedef map<string, FMOD::Sound*> SoundMap;
typedef map<int, FMOD::Channel*> ChannelMap;
typedef map<string, FMOD::Studio::EventInstance*> EventMap;
typedef map<string, FMOD::Studio::Bank*> BankMap;

class AudioEngine : public Systems::System 
{
public:

	AudioEngine();
	~AudioEngine();

	virtual void Init();
	virtual void Update(float dt);
	virtual void ShutDown();
	void ErrorCheck(FMOD_RESULT result);

	bool IsBankLoaded(const string& BankName);
	bool IsEventLoaded(const string& EventName);
	bool IsSoundLoaded(const string& SoundName);
	void LoadBank(const string& BankName, FMOD_STUDIO_LOAD_BANK_FLAGS flags = FMOD_STUDIO_LOAD_BANK_NORMAL);
	void LoadEvent(const string& EventName);
	void LoadSound(const string& SoundName, bool Looping = false, bool Stream = false);
	void UnLoadSound(const string& SoundName);
	int PlaySound_(const string& SoundName, float VolumedB = 0.0f, int channelId = 0);
	void PlayEvent(const string& EventName);
	void StopChannel(int ChannelId);
	void StopEvent(const string& EventName, bool Immediate = false);
	void GeteventParameter(const string& EventName, const string& ParameterName, float* parameter);
	void SetEventParameter(const string& EventName, const string& ParameterName, float Value);
	void PauseChannel(const int ChannelId);
	void PauseEvent(const string& EventName);
	void PauseEngine();
	void UnPauseEngine();
	bool IsChannelPaused(const int ChannelId);
	bool IsEventPaused(const string& EventName);
	void StopAllChannels();
	void StopAllAudio();
	void SetEventVolume(string EventName, float Volume);
	void SetChannelvolume(const int ChannelId, float VolumedB);
	bool IsPlaying(int ChannelId) const;
	bool IsEventPlaying(const string& EventName) const;
	float dbToVolume(float dB);
	float VolumeTodb(float volume);

private:
	FMOD::Studio::System* StudioSystem;
	FMOD::System* System;

	int NextChannelId;

	BankMap Banks;
	EventMap Events;
	SoundMap Sounds;
	ChannelMap Channels;
};

typedef std::shared_ptr<AudioEngine> AudioEnginePtr;

#endif

