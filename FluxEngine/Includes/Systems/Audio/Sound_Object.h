/******************************************************************************/
/*!
\file   Sound_Object.h
\author Deni Van Winkle
\par    email: deni.vanwinkle\@digipen.edu
\par    DigiPen login: deni.vanwinkle
\par    Course: GAM200
\par    Project: Shisan

Copyright � 2017 DigiPen (USA) Corporation.
*/
/******************************************************************************/
#ifndef Sound_Object
#define Sound_Object

#include "Vector2.h"
#include <string>
using namespace std;

class SoundObject
{
public:
	SoundObject(string sound_name, Vec2<float, float> pos, float volume);
	SoundObject(string sound_name, string bank_name, Vec2<float, float> pos, float volume);
	SoundObject(SoundObject &original);

	void Update();
	Vec2<float, float> GetPosition();
	void SetPosition(Vec2<float, float> pos);
	void SetMaxVolume(float max);
	void StopSound();
	SoundObject& operator=(const SoundObject& original);

private:
	bool studio;
	int ChannelId;
	float MaxVolume;
	string Sound_Name;
	string Bank_Name;
	Vec2<float, float> Position;
	AudioEngine* audioEngine;

	void StudioUpdate();
	void SoundUpdate();
	float VolumeByDistance();
};



#endif
