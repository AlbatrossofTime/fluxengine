/**************************************************************************/
/*!
\file   ShaderLoader.h
\author
\par
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief

Functions include:

*/
/**************************************************************************/
#pragma once
#include "stdafx.h"
#ifndef SHADERLOADER_H
#define SHADERLOADER_H

#include "glew.h"

GLuint LoadShader(const char *vertex_path, const char *fragment_path);
GLuint LoadComputeShader(const char *compute_path);

#endif