/**************************************************************************/
/*!
\file   Graphics.h
\author
\par
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief

Functions include:

*/
/**************************************************************************/
#pragma once
#include "stdafx.h"
#include "System.h"
#include "glew.h"
#include "SOIL.h"

class Graphics : public Systems::System
{
public:
	Graphics();
	//virtual ~Graphics();
	virtual void Init();
	virtual void Update(float dt);
	virtual void ShutDown();

	//SLR TODO: Presentation temp storage
	unsigned int VBO;
	unsigned int VAO;
	unsigned int IBO;
  unsigned int texture;
  unsigned char *data;
	unsigned int vertexShader;
	unsigned int fragmentShader;
	unsigned int shaderProgram;
  GLint posAttrib;
  GLint texAttrib;

	// SLR TODO: temp
	void Draw();

	char* vertexShaderSource;
	char* fragmentShaderSource;

private:
  GLuint  render_prog;
};

typedef std::shared_ptr<Graphics> GLGraphicsPtr;