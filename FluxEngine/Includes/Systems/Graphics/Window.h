#pragma once
#include "stdafx.h"
#include "System.h"
#include "glew.h"
#include "SDL.h"
#include "SDL_opengl.h"
#include <memory> // For shared pointer typedef at bottom
//#include <glm-0.9.9-a1\glm\vec2.hpp>

/**************************************************************************/
/*!
\file   Window.h
\author
\par
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief

Functions include:

*/
/**************************************************************************/



class Window : public Systems::System {
public:
  Window();
  virtual void Init();
  virtual void Update(float dt);
  virtual void ShutDown();

//private:
    // SDL Event Functions
    void PollEvents();
    void PollWindowEvent(SDL_Event &currentEvent);
    // SLR TODO: Input functionality below.
    void PollKeyEvent(SDL_Event &currentEvent);
    void PollMouseEvent(SDL_Event &currentEvent);

	struct Mouse {
		//glm::vec3 position;
		bool LPressed;
		bool LReleased;
		bool LTrigger;
		bool RPressed;
		bool RReleased;
		bool RTrigger;
	};

    // Window Data and SDL Structs
    char* title;
    int windowWidth;
    int windowHeight;
    SDL_Window      *win;
    SDL_GLContext   GLcontext; 
    SDL_Event       events; 
    bool            fullScreen;
    Mouse			theMouse;
};

typedef std::shared_ptr<Window> WindowPtr;