/**************************************************************************/
/*!
\file   GameObject.h
\author Josh Ibrahim
\par    email: j.ibrahim\@digipen.edu
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief
responsible for creating and destroying game objects
Functions include:

*/
/**************************************************************************/
#pragma once
#include "stdafx.h"
#include "GameObjectUmbrella.h"
#include <vector>
/**
* \brief
*  max number of components
*/
#define MAX_COMPONENTS Component::C_TYPE::C_Max_Num


class GameObject
{
public:
  GameObject(): m_ID(0), m_name(std::string("UNNAMED")) {};
  virtual ~GameObject();

  void AddComponent(Component* p_component, Component::C_TYPE type);
  void RemoveComponent();

  void SetName(std::string);

  const std::string& GetName();

  void SetID(unsigned int id);

  const unsigned int& GetID();

  template <class T>
  T* GetComponent(Component::C_TYPE type)
  {
    if (CheckIfComponentExists(type))
      return NULL;

    return reinterpret_cast<T*>(cList[type]);
  }

  bool CheckIfComponentExists(unsigned int type);

  virtual void Update();
protected:
  unsigned int m_ID;
  std::string m_name;

private:
  /*helper functions*/
  std::vector<Component* > cList;

  //members
  bool CLA[MAX_COMPONENTS];// component list array!
};