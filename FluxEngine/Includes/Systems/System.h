/**************************************************************************/
/*!
\file   System.h
\author
\par
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief

Functions include:

*/
/**************************************************************************/
#pragma once
#include "stdafx.h"
#ifndef _SYSTEM_H
#define _SYSTEM_H

#include <string>
#include <memory>

namespace Systems
{

  // metadata enum for system types
  enum SystemType
  {
    Sys_Invalid,
    Sys_Window,
    Sys_Input,
    Sys_Graphics,
    Sys_Audio,
    Sys_GameObjectManager,
    Sys_JCS,
    Sys_Component,
    Sys_GameTimer,
    Sys_MAX
  };

  class System
  {
    friend class FluxEngine;
  public:
    System(std::string &name, SystemType type)
      : _name(name), _type(type) {};
    virtual ~System() {};

    SystemType GetType();

    // Pure virtual functions
    virtual void Init() = 0;
    virtual void Update(float) = 0;
    virtual void ShutDown() = 0;

    //private:
      // Do not permit Systems to be created without metadata.
    System() = delete;

    // System Metadata
    std::string _name;
    SystemType  _type;

    //System(const SystemData& data); 
    //SystemType m_SystemType;
  };

  typedef std::shared_ptr<System> SystemPtr;
#endif // !_SYSTEM_H
}