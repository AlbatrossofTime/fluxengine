#pragma once
#include <stdio.h>
#include <fstream>
typedef enum
{
	Error,
	Info,
	Debug

} TraceAmount;

void  Trace(TraceAmount amount, const char *format, ...);
void Trace_Init(void);
void SetTrace(TraceAmount amount);
void Trace_Shutdown(void);