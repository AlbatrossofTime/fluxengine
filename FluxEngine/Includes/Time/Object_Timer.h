/******************************************************************************/
/*!
\file   Object_Timer.h
\author Deni Van Winkle
\par    email: deni.vanwinkle\@digipen.edu
\par    DigiPen login: deni.vanwinkle
\par    Course: GAM200
\par    Project: Shisan

Copyright � 2017 DigiPen (USA) Corporation.
*/
/******************************************************************************/
#include <chrono>

using namespace std;

class OTimer
{
public:
	OTimer(bool Minutes);


private:
	chrono::steady_clock::time_point TimerStart;
	chrono::steady_clock::time_point CurrentTime;

	float TimeLimit;
	bool Minutes;
	chrono::duration timetype;
};
