/******************************************************************************/
/*!
\file   Time.h
\author Deni Van Winkle
\par    email: deni.vanwinkle\@digipen.edu
\par    DigiPen login: deni.vanwinkle
\par    Course: GAM200
\par    Project: Shisan

Copyright � 2017 DigiPen (USA) Corporation.
*/
/******************************************************************************/
#ifndef SYSTEMTIME_H
#define SYSTEMTIME_H

#include <chrono>


class Time_
{
public:
	Time_();
	~Time_();

	float GetDT();
	float Time_Since_Start();
	float Update();


private:
	std::chrono::steady_clock::time_point System_Start_Time;
	std::chrono::steady_clock::time_point Last_Loop_Time;
	std::chrono::steady_clock::time_point Now;
	float dt;
};

#endif