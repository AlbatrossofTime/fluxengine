//#include "stdafx.h"
// SLR : There has got to be a better way to organize this.
//       Gonna think about it.
/**************************************************************************/
/*!
\file   SystemsUmbrella.h
\author
\par
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief

Functions include:

*/
/**************************************************************************/
#pragma once
#include "Window.h"
#include "Graphics.h"
#include "Audio_Engine.h"