#include "Spaces.h"



/**
 * \brief 
 *  Create space by name
 *  listen to allen, always.
 * \param name 
 */
Spaces::Spaces(std::string &name):level_(name)
{
}

/**
 * \brief 
 *  Get name of space
 * \return 
 */
std::string Spaces::GetName()
{
  return level_.GetLevel()->GetName();
}

/**
 * \brief
 *  Add system to system_ vec
 * \param sys 
 */
void Spaces::addSystem(Systems::System*& sys)
{
  systems_.push_back(sys);
}

/**
 * \brief 
 * Removes a System from Systems vector.
 * \param sys 
 */
void Spaces::removeSystem(Systems::System*& sys)
{
  for (std::vector<Systems::System*>::iterator it = systems_.begin(); it != systems_.end(); ++it)
  {
    if (sys->GetType() == (*it)->GetType())
    {
      it = systems_.erase(it); //pointing to next location
      break; // only one system added?
    }
  }

  /*delete?*/
}

/**
 * \brief 
 * \param game_object 
 */
void Spaces::addGameObject(GameObject*& game_object)
{
  objects_.push_back(game_object);
}

/**
 * \brief 
 *  Removes a GameObject from Objects_ vector.
 * \param game_object_name 
 */
void Spaces::removeGameObject(GameObject*& pObj )
{
  for (std::vector<GameObject*>::iterator it = objects_.begin(); it != objects_.end(); ++it)
  {
    if (pObj->GetID() == (*it)->GetID())
    {
      it = objects_.erase(it);
      //--it;
      break;
    }
  }

  /*delete?*/
}


/**
 * \brief 
 */
Spaces::~Spaces()
{
}
