#include <Physics.h>

//SGS: Below are the getter and setter functions for the physics component
float Physics::GetVelocity()
{
  return this->velocity;
}

float Physics::GetAcceleration()
{
  return this->acceleration;
}

float Physics::GetOrientation()
{
  return this->orientation;
}

Collider Physics::GetCollider()
{
  return this->CollisionBox;
}

void Physics::SetVelocity(float velocity)
{
  this->velocity = velocity;
}

void Physics::SetAcceleration(float acceleration)
{
  this->acceleration = acceleration;
}

void Physics::SetOrientation(float orientation)
{
  this->orientation = orientation;
}

void Physics::SetCollider(Collider collisionbox)
{
  this->CollisionBox = collisionbox;
}