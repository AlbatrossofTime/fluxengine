//#include <glm.hpp>
#include "Vector2.h"
#include <Trace.h>
#include <Collider.h>
#include <CircleCollision.h>

float Distance_SLOW(Vec2<> center1, Vec2<> center2)
{
  float x1 = center1.x;
  float x2 = center2.x;
  float y1 = center1.y;
  float y2 = center2.y;


  float result = sqrt(((x2 - x1)*(x2 - x1)) - ((y2 - y1) * (y2 - y1)));
  return result;

}

//SGS: Returns the distance between two points squared. Faster than the method above but needs to be compared to the square of radiuses
float Distance_FAST(Vec2<> center1, Vec2<> center2)
{
  float x1 = center1.x;
  float x2 = center2.x;
  float y1 = center1.y;
  float y2 = center2.y;


  float result = (((x2 - x1)*(x2 - x1)) - ((y2 - y1) * (y2 - y1)));
  return result;
}


void CollisionCheckSLOW(Collider collider1, Collider collider2)
{
  // calculate the distance between the two objects
  // subtract that distance from the both radiuses 
  // if distance between them is smaller than the sum of the radiuses then theyre colliding 

  float radius1 = collider1.radius;
  float radius2 = collider2.radius;
  Vec2<> center1 = collider1.center;
  Vec2<> center2 = collider2.center;


  if ((radius1 + radius2) >= Distance_SLOW(center1, center2))
    Trace(Debug, "Collision Detected!\n");
}


void CollisionCheckFAST(Collider collider1, Collider collider2)
{
	// calculate the distance between the two objects
	// subtract that distance from the both radiuses 
	// if distance between them is smaller than the sum of the radiuses then theyre colliding 

	float radius1 = collider1.radius;
	float radius2 = collider2.radius;
	Vec2<> center1 = collider1.center;
	Vec2<> center2 = collider2.center;


	if (((radius1 + radius2) *(radius1 + radius2)) >= Distance_FAST(center1, center2))
		Trace(Debug, "Collision Detected!\n");
}