#include "SquareMesh.h"


void SquareMesh::Set(Vec2<> TopR, Vec2<> TopL, Vec2<> BotR, Vec2<> BotL)
{
	this->Vertices[0] = TopR;
	this->Vertices[1] = TopL;
	this->Vertices[2] = BotR;
	this->Vertices[3] = BotL;
}

std::vector<Vec2<>> SquareMesh::Get(void)
{
	return this->Vertices;
}