#include "component.h"
/**************************************************************************/
/*!
\file   component.cpp
\author Josh Ibrahim
\par    email: j.ibrahim\@digipen.edu
\par    DigiPen login: j.ibrahim
\par    Course: CS180
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved. 
\date   9/28/2017
\brief

Functions include:

*/
/**************************************************************************/

/**
 * \brief 
 * \param type 
 */
Component::Component(const C_TYPE type):mType(type),mOwner(0)
{
}

/**
 * \brief 
 */
Component::~Component()
{
}



