//#include "stdafx.h"
//#include "stdafx.h"
#include <Transform.h>


template <typename T>
void ignore(T &&)
{}


//SGS: Once Game objects are implemented this will be replaced
Transform GameObjectID;


void Transform::SetPosition(uint id, Vec2<> position)
{
  //SGS: Should look something like GameObject(id)->transform.position = position
  this->position = position;
  ignore(id);
}

Vec2<> Transform::GetPosition()
{
  return this->position;
}

void Transform::SetScale(uint id, Vec2<> scale)
{
  //SGS: Should look something like GameObject(id)->transform.scale = scale
  GameObjectID.position = scale;
  ignore(id);
}

Vec2<> Transform::GetScale(uint id)
{
  return this->scale;
  ignore(id);
}

void Transform::SetRotation(uint id, float rotation)
{
  //SGS: Should look something like GameObject(id)->transform.rotation = rotation
  GameObjectID.rotation = rotation;
  ignore(id);
}

float Transform::GetRotation(uint id)
{
  return this->rotation;
  ignore(id);
}

void Transform::SetisDirty(uint id, bool is_it_dirty)
{
  //SGS: Should look something like GameObject(id)->transform.isDirty = is_it_dirty
  GameObjectID.isDirty = is_it_dirty;
  ignore(id);
}

bool Transform::GetisDirty(uint id)
{
  return this->isDirty;
}

void Transform::TransformUpdate(uint id)
{
  //SGS: GameObjectID = id passed in
  ignore(id);

  //SGS: Create the new scale matrix
  Matrix3 scale;
  scale[0][0] = GameObjectID.scale.x;
  scale[1][1] = GameObjectID.scale.y;

  //SGS: Create The matrix to move the object to the origin
  Matrix3 MoveToOrigin;
  MoveToOrigin[0][0] = 1;
  MoveToOrigin[1][1] = 1;
  MoveToOrigin[0][2] = -GameObjectID.position.x;
  MoveToOrigin[1][2] = -GameObjectID.position.y;

  //SGS: Create the translate and rotate matrix simultaneously
  Matrix3 TranslateRotate;
  TranslateRotate[0][0] = cos(rotation);
  TranslateRotate[0][1] = -sin(rotation);
  TranslateRotate[0][2] = position.x;
  TranslateRotate[1][0] = sin(rotation);
  TranslateRotate[1][1] = cos(rotation);
  TranslateRotate[1][2] = position.y;

  //SGS: Create the overall update matrix
  Matrix3 updateMatrix;
  updateMatrix = (TranslateRotate * scale) * MoveToOrigin;


  //SGS: Update the current matrix of the object's transform component
  GameObjectID.CurrentTransform *= updateMatrix;
  return;
}


void Transform::TranslateByVec(Vec2<> translationvector)
{
  position.x += (translationvector.x);
  position.y += (translationvector.y);
}


/*
void Update()
{

}*/