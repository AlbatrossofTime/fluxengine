/**************************************************************************/
/*!
\file   JCS.cpp
\author Josh Ibrahim
\par
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved\date   9/28/2017
\brief
  responsible for serialization, and deserialization.
Functions include:

*/
/**************************************************************************/
#include "JCS.h"
#include "Engine.h"
#include "GameObjectManager.h"
#include "Physics.h"

/**
 * \brief
 * \param error_msg
 */
JCSException::JCSException(std::string const& error_msg) : reason_(error_msg)
{
}

/**
 * \brief
 * \return
 */
const char* JCSException::what()
{
  std::cerr << "JCS crashes AT  : " + reason_ << std::endl;
  std::cout << "JCS crashes AT  : ";
  return reason_.c_str();
}

/**S
 * \brief
 * \param type
 */
JCS::JCS(const Systems::SystemType type, std::string extenstion) : Systems::System(std::string("Josh Custom Serializer"), type), ext_(extenstion)
{
}


/**
 * \brief
 */
JCS::~JCS()
{
}

/**
 * \brief
 *  Writes game object to a file. Specified by the ext string function the members.
 * \param o
 */
void JCS::SaveObjtxt(GameObject*& o)
{
  std::ofstream outfile;

  outfile.open(o->GetName() + ext_);

  if (outfile.is_open())
  {
    /*Write Members*/
    outfile << " Object:" + o->GetName()  << std::endl;

    /*Write Components*/ //to many errors atm to debug keeping it simple for round 1
    //WriteTransform(&outfile, *o);
     WritePhysics(outfile, o);

     //done writing close file
     outfile.close();
     
    /*delete object*/
    //TODO REMOVE! THIS IS FOR DEBUG
     //GETSYS(GameObjectManager)->Delete(o);
  }
  else
  {
    throw(JCSException(o->GetName() + " :" + "FILE NOT OPENED"));
  }
}



/**
 * \brief
 *  Opens object by name, and returns to the game object pointer
 * \param filename
 * \return
 */
GameObject* JCS::LoadObjtxt(std::string filename)//TODO LINK UP TO GameObjectManager VECTOR
{
  GameObject* o = NULL;

  std::ifstream infile;

  infile.open(filename + ext_);

  if (infile.is_open())
  {
    try {
      GETSYS(GameObjectManager)->Create(o, filename); // get new game object

      std::string str;
      std::vector<std::string> v;
      
      while (infile >> str ) //loop through file and tokenize str by delimiter :
      {
        getline(infile, str, ':'); //read up to the :
        getline(infile, str);     //read digit
        v.push_back(str);        //add to vector
      }

      //done reading close file
      infile.close();

      /*Read in Components*/
      //ReadTransform(&infile, &tokenizer, o);
      ReadPhysics(v, o);

      
    }
    catch (const std::exception& e)
    {
      throw(JCSException(filename + ext_ + " :" + "OUT OF FILE READ!"), e.what());
    }
  }
  else
  {
    throw(JCSException(filename + ext_ + " :" + "FILE NOT FOUND"));
  }

  return o;
}


/**
 * \brief
 * \param outfile
 * \param o
 */
void JCS::WriteTransform(std::ofstream& outfile, GameObject*& o)
{
  //*outfile << o->GetName() + " Vec2 Scale X:" + " :" << reinterpret_cast<Transform*>(o->cList[Component::C_TYPE::C_Transform])->GetScale().x;
  //*outfile << o->GetName() + " Vec2 Scale Y:" + " :" << reinterpret_cast<Transform*>(o->cList[Component::C_TYPE::C_Transform])->GetScale().y;
  //*outfile << o->GetName() + " Rotation :" << reinterpret_cast<Transform*>(o->cList[Component::C_TYPE::C_Transform])->GetRotation();
  //*outfile << o->GetName() + " bool isDirty :" << reinterpret_cast<Transform*>(o->cList[Component::C_TYPE::C_Transform])->GetisDirty();
  /*for (unsigned int i = 0; i < 3; ++i)
  {
    *outfile << o->GetName() + " Matrix3x3 row [" << i << "] :" << (reinterpret_cast<Transform*>(o->cList[Component::C_TYPE::C_Transform])->GetMatrixRow(i).x;
    *outfile << o->GetName() + " Matrix3x3 row[" << i << "] :" << (reinterpret_cast<Transform*>(o->cList[Component::C_TYPE::C_Transform])->GetMatrixRow(i).y;
    *outfile << o->GetName() + " Matrix3x3 row[" << i << "] :" << (reinterpret_cast<Transform*>(o->cList[Component::C_TYPE::C_Transform])->GetMatrixRow(i).z;
  }*/
}

/**
 * \brief 
 * \param outfile 
 * \param o 
 */
void JCS::WritePhysics(std::ofstream& outfile, GameObject*& o)
{
  outfile << o->GetName() + " Velocity" + " :" << reinterpret_cast<Physics*>(o->cList[Component::C_TYPE::C_Physics])->GetVelocity() << std::endl;
  outfile << o->GetName() + " Acceleration:" + " :" << reinterpret_cast<Physics*>(o->cList[Component::C_TYPE::C_Physics])->GetAcceleration() << std::endl;
  outfile << o->GetName() + " Orientation:" + " :" << reinterpret_cast<Physics*>(o->cList[Component::C_TYPE::C_Physics])->GetOrientation() << std::endl;
  WriteCollider(outfile, o);
}

/**
* \brief
* \param outfile
* \param o
*/
void JCS::WriteCollider(std::ofstream& outfile, GameObject*& o)
{

  float x = reinterpret_cast<Physics*>(o->cList[Component::C_TYPE::C_Physics])->GetCollider().center.x;
  float y = reinterpret_cast<Physics*>(o->cList[Component::C_TYPE::C_Physics])->GetCollider().center.y;
  float radius = reinterpret_cast<Physics*>(o->cList[Component::C_TYPE::C_Physics])->GetCollider().radius;
  outfile << o->GetName() + " Vec2 Center X" + " :" << x << std::endl;
  outfile << o->GetName() + " Vec2 Center Y" + " :" << y << std::endl;
  outfile << o->GetName() + " Radius" + " :" << radius << std::endl;
}

/**
 * \brief
 * \param input
 * \param o
 */
void JCS::ReadTransform(std::vector<std::string> input, GameObject*& o)
{
}

/**
 * \brief 
 * \param input 
 * \param o 
 */
void JCS::ReadPhysics(std::vector<std::string> input, GameObject*& o)
{
  reinterpret_cast<Physics*>(o->cList[Component::C_TYPE::C_Physics])->SetVelocity(atof(input[0].c_str()));
  reinterpret_cast<Physics*>(o->cList[Component::C_TYPE::C_Physics])->SetAcceleration(atof(input[1].c_str()));
  reinterpret_cast<Physics*>(o->cList[Component::C_TYPE::C_Physics])->SetOrientation(atof(input[2].c_str()));

  ReadCollider(input, o);
}


/**
* \brief
* \param input
* \param o
*/
void JCS::ReadCollider(std::vector<std::string> input, GameObject*& o)
{
  Collider c; 
  c.center.x = atof(input[3].c_str());
  c.center.y = atof(input[4].c_str());
  c.radius = atof(input[5].c_str());
  reinterpret_cast<Physics*>(o->cList[Component::C_TYPE::C_Physics])->SetCollider(c);
}
