#include "Object_Timer.h"


OTimer::OTimer(bool Min, float timeLimit)
{
	Minutes = Min;
	TimeLimit = timeLimit;
	TimeLeft = timeLimit;
	On = false;
}


void OTimer::Start()
{
	On = true;
	LastTimePoint = chrono::steady_clock::now();
	CurrentTimePoint = LastTimePoint;
}

void OTimer::Pause()
{
	On = false;
}


bool OTimer::Update()
{
	if (On)
	{
		if (Minutes)
		{
			TimeLeft -= chrono::duration_cast<std::chrono::seconds>(CurrentTimePoint - LastTimePoint).count() / 60;
		}
		else
		{
			TimeLeft -= chrono::duration_cast<std::chrono::seconds>(CurrentTimePoint - LastTimePoint).count();
		}

		if (TimeLeft <= 0)
		{
			return true;
		}
	}
	return false;
}


float OTimer::GetTimeLimit()
{
	return TimeLimit;
}


float OTimer::GetTimeLeft()
{
	return TimeLeft;
}