/******************************************************************************/
/*!
\file   Time.cpp
\author Deni Van Winkle
\par    email: deni.vanwinkle\@digipen.edu
\par    DigiPen login: deni.vanwinkle
\par    Course: GAM200
\par    Project: Shisan

Copyright � 2017 DigiPen (USA) Corporation.
*/
/******************************************************************************/
#include "SystemTime.h"

using namespace std;

/**************************************************************************/
/*!
\brief
Creates the Time system and gets the starting points for it.

\return
void
*/
/**************************************************************************/
Time_::Time_()
{
  System_Start_Time = chrono::steady_clock::now();
  Last_Loop_Time = chrono::steady_clock::now();
  dt = 0.0f;
}


/**************************************************************************/
/*!
\brief
Deletes the point to the beginning time.

\return
void
*/
/**************************************************************************/
Time_::~Time_()
{
}


/**************************************************************************/
/*!
\brief
Gets the current value of Dt for the last cycle, incase a system for some
reason can't access dt from an uppersystem.

\return
The current delta time for the last cycle.
*/
/**************************************************************************/
float Time_::GetDT()
{
  return dt;
}


/**************************************************************************/
/*!
\brief
Calculates the dt for all of the other systems to use.

\return
Delta time so it can be stored immediately.
*/
/**************************************************************************/
float Time_::Update()
{
  Now = chrono::steady_clock::now();
  float deltatime = chrono::duration_cast<std::chrono::milliseconds>(Now - Last_Loop_Time).count();

  dt = deltatime / 1000;

  Last_Loop_Time = Now;
  return dt;
}


/**************************************************************************/
/*!
\brief
Creates teh Time system and gets the starting points for it.

\return
void
*/
/**************************************************************************/
float Time_::Time_Since_Start()
{
  Now = chrono::steady_clock::now();
  return chrono::duration_cast<std::chrono::minutes>(Now - System_Start_Time).count();
}
