#include "Trace.h"
#include <Windows.h>
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <fstream>

using namespace std;
ofstream traceFile;

static TraceAmount TRACEAMOUNT;
static const char *TraceString[] = { "Error", "Info", "Debug" };
typedef struct Trace
{
	char buffer[2000];
	TraceAmount type;
} TraceData;


void Trace_Init(void)
{
	  //SGS: opens the trace log 
	ofstream traceFile ("trace.log");
	  //SGS: sets the default trace level
	TRACEAMOUNT = Info;
	if (traceFile)
	{
		return;
	}
	else {
		printf_s("Error Creating File");
		return;
	}
}

void SetTrace(TraceAmount amount)
{
	TRACEAMOUNT = amount;
}

void Trace(TraceAmount amount, const char *format, ...)
{
	//SGS: opens the trace log, there has got to be a way to only open the file once 
	//SGS: I've spent too long on this and I'm gonna leave it like this for a while
	ofstream traceFile("trace.log");
	TRACEAMOUNT = amount;
	va_list args;
  // SLR - if you are creating a 2000 char buffer every time somebody uses the trace system it might run a little slow.
	char buffer[2000];
	if (traceFile)
	{
		va_start(args, format);
		  //SGS: Don't print to file if marked as verbose
		if (TRACEAMOUNT == Error || TRACEAMOUNT == Info)
		{
			if (traceFile.is_open())
			{
				//std::cout << buffer << std::endl;
				//sprintf(buffer, "%s: ", TraceString[amount]);
				(traceFile) << buffer;
			}
		}
		//SGS: Always print to the screen
		printf("   %s: ", TraceString[amount]);

		  //SGS: Print to file if error or info tag
		if (TRACEAMOUNT == Error || TRACEAMOUNT == Info)
		{
			if (traceFile.is_open())
			{
				//std::cout << buffer << std::endl;
				//sprintf(buffer, format, args);
				traceFile << buffer;
			}
		}
		  //SGS: Always print to screen
		vprintf_s(format, args);
		printf("\n");

		if (TRACEAMOUNT == Error || TRACEAMOUNT == Info)
			traceFile << "\n";

		// Stops Looking
		va_end(args);
		return;
	}
	printf("Trace Failed :(\n");
	return;
}


void Trace_Shutdown(void)
{
	if (traceFile)
		traceFile.close();
}

