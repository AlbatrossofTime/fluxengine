#include "Engine.h"
#include "SystemsUmbrella.h"
#include <iostream>
#include <cassert>
#include "windows.h"
#include "GameObjectManager.h"
#include "Trace.h"
#include <Transform.h>
#include <collider.h>
#include <CircleCollision.h>
#include "Audio_Engine.h"
#ifndef _STRING_H
  #include <string>
#endif // !_STRING_H

#ifndef _DELETEMACRO_H
  #include "deletemacros.h"
#endif // !_DELETEMACRO_H

//********************************************
//SGS: DEBUG COLLISIONS
Transform DebugTransform;
Collider ColliderObject;
Collider ColliderOrigin;

int movementcounter = 0;
int Multiplier = 1;

//**********************************************

/**
 * \brief 
 */
EngineState FluxEngine::m_EngineState = EngineState::Invalid;

// Global Pointer for access to the Engine GameObject
/**
 * \brief 
 */
FluxEngine* ENGINE = nullptr;

void FluxEngine::InitPlayerObj()
{
  // playerObject initialization
  playerObject = new tempStateHolder;
  playerObject->stateUp = false;
  playerObject->stateDown = false;
  playerObject->stateLeft = false;
  playerObject->stateRight = false;
  playerObject->width = .5f;
  playerObject->height = .5f;

  playerObject->position = Vec2<>(0.0f, 0.0f);

  playerObject->mesh.push_back(Vec2<>(playerObject->position.x + playerObject->width, playerObject->position.y + playerObject->height));
  playerObject->mesh.push_back(Vec2<>(playerObject->position.x + playerObject->width, playerObject->position.y - playerObject->height));
  playerObject->mesh.push_back(Vec2<>(playerObject->position.x - playerObject->width, playerObject->position.y + playerObject->height));
  playerObject->mesh.push_back(Vec2<>(playerObject->position.x - playerObject->width, playerObject->position.y - playerObject->height));

  playerObject->openGLContainer.push_back(playerObject->mesh[0]);
  playerObject->openGLContainer.push_back(Vec2<>(1.0f, 0.0f));
  playerObject->openGLContainer.push_back(playerObject->mesh[1]);
  playerObject->openGLContainer.push_back(Vec2<>(1.0f, 1.0f));
  playerObject->openGLContainer.push_back(playerObject->mesh[2]);
  playerObject->openGLContainer.push_back(Vec2<>(0.0f, 0.0f));
  playerObject->openGLContainer.push_back(playerObject->mesh[3]);
  playerObject->openGLContainer.push_back(Vec2<>(0.0f, 1.0f));

  //playerObject.sprite = "";
}

/**
 * \brief 
 */
FluxEngine::FluxEngine()
{
  // Only one Engine object allowed.
  assert(ENGINE == nullptr);
  m_EngineState = EngineState::Constructing;
  dt = 0;

  FACTORY = new GameObjectFactory;
  // Main Menu Space
  FluxSpaces.push(new SeanSpace(std::string("Main Menu"), Space_MainMenu));

  InitPlayerObj();
}

void FluxEngine::calcMesh() {
	playerObject->openGLContainer[0] = Vec2<>(playerObject->position.x + playerObject->width, playerObject->position.y + playerObject->height);
	playerObject->openGLContainer[2] = Vec2<>(playerObject->position.x + playerObject->width, playerObject->position.y - playerObject->height);
	playerObject->openGLContainer[4] = Vec2<>(playerObject->position.x - playerObject->width, playerObject->position.y + playerObject->height);
	playerObject->openGLContainer[6] = Vec2<>(playerObject->position.x - playerObject->width, playerObject->position.y - playerObject->height);
}

/**
 * \brief 
 */
FluxEngine::~FluxEngine()
{
  m_EngineState = EngineState::Destroying;
  for (auto sys : Systems)
    sys->ShutDown();

  while(FluxSpaces.top())
    FluxSpaces.pop();

  delete FACTORY;
  delete playerObject;
}

/**
 * \brief 
 */
void FluxEngine::GameLoop()
{
  // SLR: Where is GetTickCount() ?
  //srand(GetTickCount());

  /*MSG msg = {};*/

  m_EngineState = EngineState::Running;
  
    // SLR : Some sort of Windows API Messaging thing?
  while (/*msg.message != WM_QUIT && */m_EngineState == EngineState::Running)
  {
    // Updating the systems.
    for (auto sys : Systems)
        sys->Update(dt);
  }
}

/**
 * \brief 
 */
void FluxEngine::Initialize()
{
  m_EngineState = EngineState::Init;
  
  // SLR: Temporary Objects for First Presentation

  // Add all systems needed for FluxEngine to work.
  Systems.push_back(Systems::SystemPtr(new Window));
  Systems.push_back(Systems::SystemPtr(new Graphics));
  Systems.push_back(Systems::SystemPtr(new AudioEngine));
  Systems.push_back(Systems::SystemPtr(new GameObjectManager));

  // Loop through Systems bucket and initialize all Systems. 
  // C++11 style range-based for loop.
  // All Systems currently running on Engine.
  for (auto sys : Systems)
      sys->Init();
  // Push initial Main Menu space onto the Space stack.
  
}

/**
 * \brief 
 */
void FluxEngine::ShutDown() {
  for(auto sys : Systems)
    sys->ShutDown();

  Systems.clear();

  delete FluxSpaces.top();
  FluxSpaces.pop();

  ENGINE = nullptr;
}

/**
 * \brief 
 */
void FluxEngine::Stop()
{
  m_EngineState = EngineState::ShuttingDown;
}