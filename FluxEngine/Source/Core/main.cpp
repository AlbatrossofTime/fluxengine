/**************************************************************************/
/*!
\file   main.cpp
\author
\par
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief

Functions include:

*/
/**************************************************************************/

#include "stdafx.h"
#include "Engine.h"
#include "Trace.h"

/**
 * \brief 
 * Connection to Engine Ptr in Engine.cpp
 */
extern FluxEngine* ENGINE;

/**
 * \brief 
 *  Main entry point into program
 * \param argc 
 * \param args 
 * \return 
 */
int main(int argc, char* args[]) {
	ENGINE = new FluxEngine();

  // Set Up Engine, create Systems
  ENGINE->Initialize();

  //SGS: Initialize trace 
  Trace_Init();
  Trace(Info, "Trace system Initialized.\n");

  // Game Loop
  ENGINE->GameLoop();

  //SGS: Shutdown Trace
  Trace_Shutdown();

  // Engine Shutdown procedure, remove systems in reverse order
  ENGINE->ShutDown();

	// Clean-up
	delete ENGINE;

	//return result;
	return 0;
}