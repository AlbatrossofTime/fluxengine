#include "GameObjectFactory.h"
#include "Trace.h"
#include <cassert>

GameObjectFactory::GameObjectFactory() : GameObjectTotal(0), LastID(0)
{
}

GameObjectFactory::~GameObjectFactory()
{
}

void GameObjectFactory::Create(std::string name)
{
  ++GameObjectTotal;
  ++LastID;
  
  GameObject* pObj = new GameObject();
  if (!pObj)
    Trace(Debug, "GameObject creation failed!");

  pObj->SetName(name);
  pObj->SetID(LastID);
  Trace(Debug, "GameObject created! Name: %s ID: %i \n", pObj->GetName().c_str(), pObj->GetID());
  // SLR TODO
  OM.push_back(pObj);
}

void GameObjectFactory::Delete(GameObject * pObj)
{
  for (std::vector<GameObject*>::iterator it = OM.begin(); it != OM.end(); ++it)
  {
    if (pObj->GetID() == (*it)->GetID())
    {
      for (unsigned int i = 0; i < Component::C_TYPE::C_Max_Num; ++i)
      {
        if ((*it)->CheckIfComponentExists(i))
        {
          pObj->RemoveComponent();
        }
      }
      (*it)->RemoveComponent();
      delete (*it);
    }
  }
  --GameObjectTotal;
}
