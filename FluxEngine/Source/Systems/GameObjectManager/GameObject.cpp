#include "GameObject.h"
/**************************************************************************/
/*!
\file   GameObject.cpp
\author Josh Ibrahim
\par
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief

Functions include:

*/
/**************************************************************************/



  /**
   * \brief
   */
/*GameObject::GameObject() : m_ID(0)
{
  for (int i = 1; i < Component::C_TYPE::C_Max_Num; ++i)
  {
    CLA[i - 1] = 0;
  }
}*/


/**
 * \brief
 */
GameObject::~GameObject()
{
}

/**
 * \brief
 * \param name
 */
void GameObject::SetName(std::string name)
{
  m_name += name;
}

/**
 * \brief
 * \return
 */
const std::string& GameObject::GetName()
{
  return m_name;
}

/**
 * \brief
 * \param id
 */
void GameObject::SetID(unsigned int id)
{
  m_ID = id;
}


/**
 * \brief
 * \return
 */
const unsigned int& GameObject::GetID()
{
  return m_ID;
}




/**
 * \brief
 */
void GameObject::Update()
{
}

/**
 * \brief
 * \param type
 * \return
 */
bool GameObject::CheckIfComponentExists(unsigned int type)
{
  return CLA[type];
}

/**
 * \brief
 * \param p_component
 */
void GameObject::AddComponent(Component* p_component, Component::C_TYPE type)
{
  p_component->mOwner = this;
  cList.push_back(p_component);
  CLA[type] = 1;
}

/**
 * \brief
 */
void GameObject::RemoveComponent()
{
  delete cList.back();
  cList.pop_back();
}