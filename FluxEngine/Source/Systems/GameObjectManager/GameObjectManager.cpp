/**************************************************************************/
/*!
\file   GameObjectManager.cpp
\author Josh Ibrahim
\par    email: j.ibrahim\@digipen.edu
\par    DigiPen login: j.ibrahim
\par    Course: CS180
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief

Functions include:

*/
/**************************************************************************/

#include "GameObjectManager.h"
#include "Transform.h"
#include "Physics.h"

/**
 * \brief 
 * \param type 
 */
GameObjectManager::GameObjectManager(const Systems::SystemType type) : Systems::System(std::string("Object Manger"), type), GameObjectTotal(0), LASTID(0)
{
}

/**
 * \brief 
 */
GameObjectManager::~GameObjectManager()
{
}

/**
 * \brief 
 */
void GameObjectManager::Init()
{
}

/**
 * \brief 
 */
void GameObjectManager::Update(float)
{
  /*
  std::vector<GameObject*>::iterator it; 
  
  for ( it = OM.begin(); it == OM.end(); ++it)
  { 
    (*it)->Update();
  }
  */
  /*
  Serializer* s;
  GameObject* pobj = NULL;
  Create(pobj, std::string("bob"));
  s->jsonSaveObj(pobj);*/
  /*GameObject* l;
  Create(l, "bob");
  l->GetComponent<Transform>(Component::C_TYPE::C_Transform)->GetMatrix(0);*/
}


/**
 * \brief 
 */
void GameObjectManager::ShutDown()
{
}

/**
 *\brief 
 *\param pObj
 *\param name
 */
// SLR - why are you trying to pass a GameObject to the function that creates a GameObject?
void GameObjectManager::Create(GameObject*& pObj , std::string name)
{
  ++GameObjectTotal;
  ++LASTID;
  pObj = new GameObject;
  pObj->SetName(name);
  pObj->AddComponent(new Transform, Component::C_Transform);
  pObj->AddComponent(new Physics, Component::C_Physics);
  pObj->SetID(LASTID);
  OM.push_back(pObj);
}

/**
 *\param pObj 
 *
 *\brief 
 *  Deletes game Object
 */
void GameObjectManager::Delete(GameObject*& pObj)
{
  --GameObjectTotal;
  for(std::vector<GameObject*>::iterator it = OM.begin(); it != OM.end(); ++it)
  {
    if ( pObj->GetID() == (*it)->GetID() )
    {
      for (unsigned int i =0; i < Component::C_TYPE::C_Max_Num; ++i)
      {
        if ((*it)->CheckIfComponentExists(i) )
        {
          pObj->RemoveComponent();
        }
      }
      (*it)->RemoveComponent();
      delete (*it);
    }
  }
}
