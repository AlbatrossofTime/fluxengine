
/**************************************************************************/
/*!
\file   System.cpp
\author
\par
\par    DigiPen login:
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief

Functions include:

*/
/**************************************************************************/
#include "System.h"
/**
 * \brief 
 * \return 
 */
namespace Systems
{

  SystemType System::GetType()
  {
    return _type;
  }

}
