#include "Matrix3.h"

Vec3<>& Matrix3::operator[](int i)
{
	return row[i];
}

const Vec3<>& Matrix3::operator[](int i) const
{
	return row[i];
}

Matrix3::Matrix3()
{
	row[0] = Vec3<>(1.0f, 0.0f, 0.0f);
	row[1] = Vec3<>(0.0f, 1.0f, 0.0f);
	row[2] = Vec3<>(0.0f, 0.0f, 1.0f);
}

Matrix3::Matrix3(const Vec3<>& Lx, const Vec3<>& Ly, const Vec3<>& W)
{
	row[0] = Lx;
	row[1] = Ly;
	row[2] = W;
}

float Matrix3::determinant(float a, float b, float c, float d)
{
	return (a * d) - (b * c);
}

Matrix3 Matrix3::transpose(Matrix3 A)
{
	Matrix3 B;
	for (int i = 0; i <= 2; i++)
	{
		for (int j = 0; j <= 2; j++)
		{
			B[i][j] = A[j][i];
		}
	}

	return B;
}

// 2D Transformations
// rot around arbitrary vec2
//Matrix3 Matrix3::Rot(float t, const Vec2<>& v)
//{
//}



Matrix3 Matrix3::Rot(float t)
{
	Matrix3 rot;

	rot[0][0] = cos(t); rot[0][1] = -sin(t);
	rot[1][0] = sin(t); rot[1][1] = cos(t);

	return rot;
}

Matrix3 Matrix3::Trans(const Vec2<>& v)
{
	Matrix3 trans;

	trans[0][2] = v.x;
	trans[1][2] = v.y;

	return trans;
}

Matrix3 Matrix3::Scale(float r)
{
	Matrix3 trans;

	trans[0][0] = trans[1][1] = r;

	return trans;
}

Matrix3 Matrix3::Scale(float rx, float ry)
{
	Matrix3 trans;

	trans[0][0] = rx;
	trans[1][1] = ry;

	return trans;
}

Matrix3 & Matrix3::operator*=(const Matrix3 & A)
{
  *this = A * *this;
  return *this;
}

/*Matrix3 Matrix3::Inverse(const Matrix3 & A)
{
	float inv = 1.0f / determinant(row[0][0], row[0][1], row[1][0], row[1][1]);

	Matrix3 returnMat();
}*/

Vec3<> operator*(const Matrix3 & A, const Vec3<>& v)
{
	Vec3<> returnVec;

	for (int i = 0; i <= 3; i++)
	{
		for (int j = 0; j <= 3; j++)
		{
			returnVec[i] += A[i][j] * v[j];
		}
	}

	return returnVec;
}

Matrix3 operator*(const Matrix3 & A, const Matrix3 & B)
{
	Matrix3 returnMatrix;

	for (int i = 0; i <= 3; i++)
	{
		for (int j = 0; j <= 3; j++)
		{
			for (int k = 0; k <= 3; k++)
			{
				returnMatrix[i][j] += A[i][k] * B[k][j];
			}
		}
	}

	return returnMatrix;
}
