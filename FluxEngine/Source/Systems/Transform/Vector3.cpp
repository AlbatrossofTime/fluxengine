#include "Vector3.h"

#include "Vector2.h"

template<typename U, typename V, typename W>
Vec3<U, V, W>::Vec3() : x(0), y(0)
{
}

template<typename U, typename V, typename W>
Vec3<U, V, W>::~Vec3()
{
}

template<typename U, typename V, typename W>
Vec3<U, V, W>::Vec3(U _x, V _y, W _z) : x(_x), y(_y), z(_z)
{
}

template<typename U, typename V, typename W>
Vec3<U, V, W>::Vec3(const Vec3 & v) : x(v.x), y(v.y), z(v.z)
{
}

template<typename U, typename V, typename W>
Vec3<U, V, W>::Vec3(const Vec3 * v) : x(v->x), y(v->y), z(v->z)
{
}

template<typename U, typename V, typename W>
void Vec3<U, V, W>::Set(U _x, V _y, W _z)
{
	x = _x;
	y = _y;
	z = _z;
}

template<typename U, typename V, typename W>
float Vec3<U, V, W>::Length() const
{
	return sqrt(x * x + y * y + z * z);
}

template<typename U, typename V, typename W>
float Vec3<U, V, W>::LengthSquared() const
{
	return (x * x + y * y + z * z);
}

template<typename U, typename V, typename W>
float Vec3<U, V, W>::Distance(const Vec3 &v) const
{
	return sqrt(((x - v.x) * (x - v.x)) + ((y - v.y) * (y - v.y)) + ((z - v.z) * (z - v.z)));
}

template<typename U, typename V, typename W>
float Vec3<U, V, W>::DistanceSquared(const Vec3 &v) const
{
	return ((x - v.x) * (x - v.x)) + ((y - v.y) * (y - v.y)) + ((z - v.z) * (z - v.z));
}

template<typename U, typename V, typename W>
float Vec3<U, V, W>::Dot(const Vec3 &v) const
{
	return x * v.x + y * v.y + z * v.z;
}

template<typename U, typename V, typename W>
Vec3<U,V,W> Vec3<U, V, W>::Cross(const Vec3 &v) const
{
	return Vec3(y * v.z - z * v.y,
		        z * v.x - x * v.z,
		        x * v.y - y * v.x);
}

template<typename U, typename V, typename W>
Vec3<U, V, W> Vec3<U, V, W>::Normal()
{
	if (Length() != 0)
		return Vec3(x / Length(), y / Length(), z / Length());
	else
		return *this;
}

template<typename U, typename V, typename W>
void Vec3<U, V, W>::Normalize()
{
	x = x / Length();
	y = y / Length();
	z = z / Length();
}

template<typename U, typename V, typename W>
Vec3<U, V, W> & Vec3<U, V, W>::operator=(const Vec3 & v)
{
	x = v.x;
	y = v.y;
	z = v.z;
	return *this;
}

template<typename U, typename V, typename W>
Vec3<U, V, W> & Vec3<U, V, W>::operator=(const float & f)
{
	x = f;
	y = f;
	z = f;
	return *this;
}

template<typename U, typename V, typename W>
Vec3<U, V, W> & Vec3<U, V, W>::operator-()
{
	x = -x;
	y = -y;
	z = -z;
	return *this;
}

template<typename U, typename V, typename W>
bool Vec3<U, V, W>::operator==(const Vec3 & v) const
{
	return (x == v.x) && (y == v.y) && (z == v.z);
}

template<typename U, typename V, typename W>
bool Vec3<U, V, W>::operator!=(const Vec3 & v) const
{
	return (x != v.x) && (y != v.y) && (z != v.z);
}

template<typename U, typename V, typename W>
Vec3<U, V, W> Vec3<U, V, W>::operator+(const Vec3 & v) const
{
	return Vec3(x + v.x, y + v.y, z + v.z);
}

template<typename U, typename V, typename W>
Vec3<U, V, W> Vec3<U, V, W>::operator-(const Vec3 & v) const
{
	return Vec3(x - v.x, y - v.y, z - v.z);
}

template<typename U, typename V, typename W>
Vec3<U, V, W> Vec3<U, V, W>::operator*(const Vec3 & v) const
{
	return Vec3(x * v.x, y * v.y,  z * v.z);
}

template<typename U, typename V, typename W>
Vec3<U, V, W> Vec3<U, V, W>::operator/(const Vec3 & v) const
{
	return Vec3(x / v.x, y / v.y, z / v.z);
}

template<typename U, typename V, typename W>
Vec3<U, V, W> & Vec3<U, V, W>::operator+=(const Vec3 & v)
{
	x += v.x;
	y += v.y;
	z += v.z;
	return *this;
}

template<typename U, typename V, typename W>
Vec3<U, V, W> & Vec3<U, V, W>::operator-=(const Vec3 & v)
{
	x -= v.x;
	y -= v.y;
	z -= v.z;
	return *this;
}

template<typename U, typename V, typename W>
Vec3<U, V, W> & Vec3<U, V, W>::operator*=(const Vec3 & v)
{
	x *= v.x;
	y *= v.y;
	z *= v.z;
	return *this;
}

template<typename U, typename V, typename W>
Vec3<U, V, W> & Vec3<U, V, W>::operator/=(const Vec3 & v)
{
	x /= v.x;
	y /= v.y;
	z /= v.z;
	return *this;
}

template<typename U, typename V, typename W>
Vec3<U, V, W> Vec3<U, V, W>::operator+(float & f)
{
	return Vec3(x + f, y + f, z + f);
}

template<typename U, typename V, typename W>
Vec3<U, V, W> Vec3<U, V, W>::operator-(float & f)
{
	return Vec3(x - f, y - f, z - f);
}

template<typename U, typename V, typename W>
Vec3<U, V, W> Vec3<U, V, W>::operator*(float & f)
{
	return Vec3(x * f, y * f, z * f);
}

template<typename U, typename V, typename W>
Vec3<U, V, W> Vec3<U, V, W>::operator/(float & f)
{
	return Vec3(x / f, y / f, z / f);
}

template<typename U, typename V, typename W>
Vec3<U, V, W> & Vec3<U, V, W>::operator+=(float & f)
{
	x += f;
	y += f;
	z += f;
	return *this;
}

template<typename U, typename V, typename W>
Vec3<U, V, W> & Vec3<U, V, W>::operator-=(float & f)
{
	x -= f;
	y -= f;
	z -= f;
	return *this;
}

template<typename U, typename V, typename W>
Vec3<U, V, W> & Vec3<U, V, W>::operator*=(float & f)
{
	x *= f;
	y *= f;
	z *= f;
	return *this;
}

template<typename U, typename V, typename W>
Vec3<U, V, W> & Vec3<U, V, W>::operator/=(float & f)
{
	x /= f;
	y /= f;
	z /= f;
	return *this;
}

template<typename U, typename V, typename W>
float& Vec3<U, V, W>::operator[](int i)
{
	return *(&x + i);
}

template<typename U, typename V, typename W>
float Vec3<U, V, W>::operator[](int i) const
{
	return *(&x + i);
}

template struct Vec3<float, float, float>;