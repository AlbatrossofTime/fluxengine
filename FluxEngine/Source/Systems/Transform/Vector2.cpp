#include "Vector2.h"

template<typename U, typename V>
Vec2<U, V>::Vec2() : x(0), y(0)
{
}

template<typename U, typename V>
Vec2<U, V>::~Vec2()
{
}

template<typename U, typename V>
Vec2<U, V>::Vec2(U _x, V _y) : x(_x), y(_y)
{
}

template<typename U, typename V>
Vec2<U, V>::Vec2(const Vec2 & v) : x(v.x), y(v.y)
{
}

template<typename U, typename V>
Vec2<U, V>::Vec2(const Vec2 * v) : x(v->x), y(v->y)
{
}

template<typename U, typename V>
void Vec2<U, V>::Set(U _x, V _y)
{
	x = _x;
	y = _y;
}

template<typename U, typename V>
float Vec2<U, V>::Length() const
{
	return sqrt(x * x + y * y);
}

template<typename U, typename V>
float Vec2<U, V>::LengthSquared() const
{
	return (x * x + y * y);
}

template<typename U, typename V>
float Vec2<U, V>::Distance(const Vec2 &v) const
{
	return sqrt(((x - v.x) * (x - v.x)) + ((y - v.y) * (y - v.y)));
}

template<typename U, typename V>
float Vec2<U, V>::DistanceSquared(const Vec2 &v) const
{
	return ((x - v.x) * (x - v.x)) + ((y - v.y) * (y - v.y));
}

template<typename U, typename V>
float Vec2<U, V>::Dot(const Vec2 &v) const
{
	return x * v.x + y * v.y;
}

//template<typename U, typename V>
//float Vec2<U, V>::Cross(const Vec2 &v) const
//{
//	return 0.0f;
//}

template<typename U, typename V>
Vec2<U, V> Vec2<U, V>::Normal()
{
	if (Length() != 0)
		return Vec2(x / Length(), y / Length());
	else
		return *this;
}

template<typename U, typename V>
void Vec2<U, V>::Normalize()
{
	x = x / Length();
	y = y / Length();
}

template<typename U, typename V>
Vec2<U,V> & Vec2<U, V>::operator=(const Vec2 & v)
{
	x = v.x;
	y = v.y;
	return *this;
}

template<typename U, typename V>
Vec2<U, V> & Vec2<U, V>::operator=(const float & f)
{
	x = f;
	y = f;
	return *this;
}

template<typename U, typename V>
Vec2<U, V> & Vec2<U, V>::operator-()
{
	x = -x;
	y = -y;
	return *this;
}

template<typename U, typename V>
bool Vec2<U, V>::operator==(const Vec2 & v) const
{
	return (x == v.x) && (y == v.y);
}

template<typename U, typename V>
bool Vec2<U, V>::operator!=(const Vec2 & v) const
{
	return (x != v.x) && (y != v.y);
}

template<typename U, typename V>
Vec2<U, V> Vec2<U, V>::operator+(const Vec2 & v) const
{
	return Vec2(x + v.x, y + v.y);
}

template<typename U, typename V>
Vec2<U, V> Vec2<U, V>::operator-(const Vec2 & v) const
{
	return Vec2(x - v.x, y - v.y);
}

template<typename U, typename V>
Vec2<U, V> Vec2<U, V>::operator*(const Vec2 & v) const
{
	return Vec2(x * v.x, y * v.y);
}

template<typename U, typename V>
Vec2<U, V> Vec2<U, V>::operator/(const Vec2 & v) const
{
	return Vec2(x / v.x, y / v.y);
}

template<typename U, typename V>
Vec2<U, V> & Vec2<U, V>::operator+=(const Vec2 & v)
{
	x += v.x;
	y += v.y;
	return *this;
}

template<typename U, typename V>
Vec2<U, V> & Vec2<U, V>::operator-=(const Vec2 & v)
{
	x -= v.x;
	y -= v.y;
	return *this;
}

template<typename U, typename V>
Vec2<U, V> & Vec2<U, V>::operator*=(const Vec2 & v)
{
	x *= v.x;
	y *= v.y;
	return *this;
}

template<typename U, typename V>
Vec2<U, V> & Vec2<U, V>::operator/=(const Vec2 & v)
{
	x /= v.x;
	y /= v.y;
	return *this;
}

template<typename U, typename V>
Vec2<U, V> Vec2<U, V>::operator+(float & f)
{
	return Vec2(x + f, y + f);
}

template<typename U, typename V>
Vec2<U, V> Vec2<U, V>::operator-(float & f)
{
	return Vec2(x - f, y - f);
}

template<typename U, typename V>
Vec2<U, V> Vec2<U, V>::operator*(float & f)
{
	return Vec2(x * f, y * f);
}

template<typename U, typename V>
Vec2<U, V> Vec2<U, V>::operator/(float & f)
{
	return Vec2(x / f, y / f);
}

template<typename U, typename V>
Vec2<U, V> & Vec2<U, V>::operator+=(float & f)
{
	x += f;
	y += f;
	return *this;
}

template<typename U, typename V>
Vec2<U, V> & Vec2<U, V>::operator-=(float & f)
{
	x -= f;
	y -= f;
	return *this;
}

template<typename U, typename V>
Vec2<U, V> & Vec2<U, V>::operator*=(float & f)
{
	x *= f;
	y *= f;
	return *this;
}

template<typename U, typename V>
Vec2<U, V> & Vec2<U, V>::operator/=(float & f)
{
	x /= f;
	y /= f;
	return *this;
}

template<typename U, typename V>
float& Vec2<U, V>::operator[](int i)
{
	return *(&x + i);
}

template<typename U, typename V>
float Vec2<U, V>::operator[](int i) const
{
	return *(&x + i);
}

template struct Vec2<float,float>;