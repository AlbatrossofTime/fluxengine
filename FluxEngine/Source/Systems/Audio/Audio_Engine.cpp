/******************************************************************************/
/*!
\file   Audio_Engine.cpp
\author Deni Van Winkle
\par    email: deni.vanwinkle\@digipen.edu
\par    DigiPen login: deni.vanwinkle
\par    Course: GAM200
\par    Project: Shisan

Copyright � 2017 DigiPen (USA) Corporation.
*/
/******************************************************************************/
#include "Audio_Engine.h"
#include <vector>


AudioEngine* Audio = nullptr;

/**************************************************************************/
/*!
\brief
Creates a new sound engine and sets the values to null.

*/
/**************************************************************************/
AudioEngine::AudioEngine() : Systems::System(std::string("Audio System"), Systems::Sys_Window)
{
	StudioSystem = NULL;
	System = NULL;

	NextChannelId = 1;
}

/**************************************************************************/
/*!
\brief
Cleans up the system after it's gone out of scope.
*/
/**************************************************************************/
AudioEngine::~AudioEngine()
 {
	ErrorCheck(StudioSystem->unloadAll());
	ErrorCheck(StudioSystem->release());
}

/**************************************************************************/
/*!
\brief
Intitalizes the audio engine by creating the pointers to the fmod systems.

\return
void
*/
/**************************************************************************/
void AudioEngine::Init()
{
	ErrorCheck(FMOD::Studio::System::create(&StudioSystem));
	ErrorCheck(StudioSystem->initialize(32, FMOD_STUDIO_INIT_LIVEUPDATE, FMOD_INIT_PROFILE_ENABLE, NULL));

	ErrorCheck(StudioSystem->getLowLevelSystem(&System));
}


/**************************************************************************/
/*!
\brief
Updates the entire sound engine and gets rid of sounds that aren't currently
playing

\param float
Delta time

\return
void
*/
/**************************************************************************/
void AudioEngine::Update(float dt) 
{
	vector<ChannelMap::iterator> StoppedChannels;

	for (auto it = Channels.begin(), itEnd = Channels.end(); it != itEnd; ++it)
	{
		bool IsPlaying = false;
		it->second->isPlaying(&IsPlaying);
		if (!IsPlaying)
		{
			StoppedChannels.push_back(it);
		}
	}
	for (auto& it : StoppedChannels)
	{
		Channels.erase(it);
	}
	ErrorCheck(StudioSystem->update());
}


/**************************************************************************/
/*!
\brief
Clears all of the channels of it's current data.

\return
void
*/
/**************************************************************************/
void AudioEngine::ShutDown()
{
	Channels.clear();
}

/**************************************************************************/
/*!
\brief
Updates the entire sound engine and gets rid of sounds that aren't currently
playing

\param FMOD_RESULT
Error messages that FMOD function throw.

\return
void
*/
/**************************************************************************/
void AudioEngine::ErrorCheck(FMOD_RESULT result)
{
	if (result != FMOD_OK) 
	{
		cout << "FMOD ERROR " << result << endl;
	}
}

/**************************************************************************/
/*!
\brief
Checks if the current bank was loaded into the system.

\param  string
The name of the bank that is being looked for.

\return
Whether the given bank has been loaded or not.
*/
/**************************************************************************/
bool AudioEngine::IsBankLoaded(const string& BankName)
{
	auto FoundIt = Banks.find(BankName);
	if (FoundIt != Banks.end()) 
	{
		return false;
	}
	return true;
}

/**************************************************************************/
/*!
\brief
Checks if the current Event was loaded into the system.

\param  string
The name of the Event that is being looked for.

\return
Whether the given Event has been loaded or not.
*/
/**************************************************************************/
bool AudioEngine::IsEventLoaded(const string& EventName)
{
	auto Foundit = Events.find(EventName);
	if (Foundit != Events.end()) 
	{
		return false;
	}
	return true;
}


/**************************************************************************/
/*!
\brief
Checks if the current sound was loaded into the system.

\param  string
The name of the sound that is being looked for.

\return
Whether the given sound has been loaded or not.
*/
/**************************************************************************/
bool AudioEngine::IsSoundLoaded(const string& SoundName)
{
	auto FoundIt = Sounds.find(SoundName);
	if (FoundIt != Sounds.end()) 
	{
		return false;
	}
	return true;
}


/**************************************************************************/
/*!
\brief
Loads a given bank file with the given settings/flags.

\param  string
The name of the bank that is being loaded into the system

\param FMOD_STUDIO_LOAD_BANK_FLAGS
The Setting on how the bank file should be loaded into the system.

\return
void
*/
/**************************************************************************/
void AudioEngine::LoadBank(const string& BankName, FMOD_STUDIO_LOAD_BANK_FLAGS flags)
{
	if (!IsBankLoaded(BankName))
	{
		FMOD::Studio::Bank* pBank;
		ErrorCheck(StudioSystem->loadBankFile(BankName.c_str(), flags, &pBank));
		if (pBank) 
		{
			Banks[BankName] = pBank;
		}
	}
}

/**************************************************************************/
/*!
\brief
Loads a given Event into the event system.

\param  string
The name of the Event to be loaded into the system.

\return
void
*/
/**************************************************************************/
void AudioEngine::LoadEvent(const string& EventName)
{
	if (!IsEventLoaded(EventName))
	{
		FMOD::Studio::EventDescription* pEventDescription = NULL;
		ErrorCheck(StudioSystem->getEvent(EventName.c_str(), &pEventDescription));
		if (pEventDescription) 
		{
			FMOD::Studio::EventInstance* pEventInstance = NULL;
			ErrorCheck(pEventDescription->createInstance(&pEventInstance));
			if (pEventInstance) 
			{
				Events[EventName] = pEventInstance;
			}
		}
	}
}

/**************************************************************************/
/*!
\brief
Loads a sound based on the file name and the bools passed in with it.

\param  string
The name of the sound to be loaded into the system.

\param bool
Whether the sound should loop or just play once.

\param bool
Whether the sound should be loaded in compressed or decompressed.

\return
void
*/
/**************************************************************************/
void AudioEngine::LoadSound(const string& SoundName, bool Looping, bool Stream)
{
	if (!IsSoundLoaded(SoundName))
	{
		FMOD_MODE eMode = FMOD_DEFAULT;
		eMode |= FMOD_2D;
		eMode |= Looping ? FMOD_LOOP_NORMAL : FMOD_LOOP_OFF;
		//Create Compressed Sample loads in the file such as .mp3 into memory and will be decompresseded at run time
		eMode |= Stream ? FMOD_CREATESTREAM : FMOD_CREATECOMPRESSEDSAMPLE;

		FMOD::Sound* Sound = nullptr;
		ErrorCheck(System->createSound(SoundName.c_str(), eMode, nullptr, &Sound));
		if (Sound) 
		{
			Sounds[SoundName] = Sound;
		}
	}
}


/**************************************************************************/
/*!
\brief
Unloades  the given sound from the system.

\param string
The name of the sound that should be unloaded from the sustm

\return
void
*/
/**************************************************************************/
void AudioEngine::UnLoadSound(const string& SoundName)
{
	auto FoundIt = Sounds.find(SoundName);
	if (FoundIt != Sounds.end()) 
	{
		return;
	}

	ErrorCheck(FoundIt->second->release());
    Sounds.erase(FoundIt);
}


int AudioEngine::PlaySound_(const string& SoundName, float VolumedB, int channelId)
{
	  // Checking if the sound already has a channelId
	int ChannelId;
	if (channelId == 0)
	{
		ChannelId = NextChannelId++;
	}
	else
	{
		ChannelId = channelId;
	}

	  // Checking if the Soudn has been loaded yet
	auto FoundIt = Sounds.find(SoundName);
	if (FoundIt == Sounds.end())
	{
		  // If the sound isn't found then its loaded into the system
		LoadSound(SoundName);
		FoundIt = Sounds.find(SoundName);
		  // Checks to make sure that it was loaded properly before continuing
		if (FoundIt == Sounds.end())
		{
			return 0;
		}
	}

	  // Creates a channel 
	FMOD::Channel* Channel = nullptr;
	ErrorCheck(System->playSound(FoundIt->second, nullptr, true, &Channel));
	if (Channel)
	{
		ErrorCheck(Channel->setVolume(VolumedB));
		ErrorCheck(Channel->setPaused(false));
		Channels[ChannelId] = Channel;
	}
	return ChannelId;
}


/**************************************************************************/
/*!
\brief
Starts the sound event after it checks whether the event is already playing.

\param string
The name of the event to be played.

\return
void
*/
/**************************************************************************/
void AudioEngine::PlayEvent(const string& EventName) 
{
	auto Foundit = Events.find(EventName);
	if (Foundit == Events.end()) 
	{
		LoadEvent(EventName);
		Foundit = Events.find(EventName);
		if (Foundit == Events.end()) 
		{
			return;
		}
	}
	Foundit->second->start();
}



void AudioEngine::StopEvent(const string& EventName, bool Immediate)
{
	auto FoundIt = Events.find(EventName);
	if (FoundIt == Events.end()) 
	{
		return;
	}

	FMOD_STUDIO_STOP_MODE eMode;
	eMode = Immediate ? FMOD_STUDIO_STOP_IMMEDIATE : FMOD_STUDIO_STOP_ALLOWFADEOUT;
	ErrorCheck(FoundIt->second->stop(eMode));
}


void AudioEngine::GeteventParameter(const string& EventName, const string& ParameterName, float* parameter)
{
	auto FoundIt = Events.find(EventName);
	if (FoundIt == Events.end())
	{
		return;
	}

	FMOD::Studio::ParameterInstance* pParameter = NULL;
	ErrorCheck(FoundIt->second->getParameter(ParameterName.c_str(), &pParameter));
	ErrorCheck(pParameter->getValue(parameter));
}


void AudioEngine::SetEventParameter(const string& EventName, const string& ParameterName, float Value)
{
	auto FoundIt = Events.find(EventName);
	if (FoundIt == Events.end())
	{
		return;
	}

	FMOD::Studio::ParameterInstance* pParameter = NULL;
	ErrorCheck(FoundIt->second->getParameter(ParameterName.c_str(), &pParameter));
	ErrorCheck(pParameter->setValue(Value));
}



void AudioEngine::StopChannel(int ChannelId)
{
	ErrorCheck(Channels[ChannelId]->stop());
}


void AudioEngine::StopAllChannels()
{
	map<int, FMOD::Channel*>::iterator it = Channels.begin();
	while (it != Channels.end())
	{
		ErrorCheck(it->second->stop());
		++it;
	}
}



/**************************************************************************/
/*!
\brief
Changes the volume of a channel by the given amount.

\param int
The channel thats having it's volume changed.

\param float
The volume between 0 and 1 that the user want the sound to be at.

\return
void
*/
/**************************************************************************/
void AudioEngine::SetChannelvolume(const int ChannelId, float Volume)
{
	auto FoundIt = Channels.find(ChannelId);
	if (FoundIt == Channels.end()) 
	{
		return;
	}

	ErrorCheck(FoundIt->second->setVolume(Volume));
}


/**************************************************************************/
/*!
\brief
Changes the volume of an Event by the given amount.

\param string
The Event that's having it's volume changed.

\param float
The volume between 0 and 1 that the user want the sound to be at.

\return
void
*/
/**************************************************************************/
void AudioEngine::SetEventVolume(string EventName, float Volume)
{
	auto FoundIt = Events.find(EventName);
	if (FoundIt == Events.end()) 
	{
		return;
	}

	ErrorCheck(FoundIt->second->setVolume(Volume));
}


/**************************************************************************/
/*!
\brief
Checks if the given Event is currently playing.

\param string
The name of the Event that's being checked.

\return
Whether the channel is playing or not.
*/
/**************************************************************************/
bool AudioEngine::IsEventPlaying(const string& EventName) const
{
	auto FoundIt = Events.find(EventName);
	if (FoundIt == Events.end()) 
	{
		return false;
	}

	FMOD_STUDIO_PLAYBACK_STATE* State = NULL;
	if (FoundIt->second->getPlaybackState(State) == FMOD_STUDIO_PLAYBACK_PLAYING) 
	{
		return true;
	}
	return false;
}


/**************************************************************************/
/*!
\brief
Checks if the given channel is playing currently.

\param int
The channel to be checked.

\return
Whether the channel is playing or not.
*/
/**************************************************************************/
bool AudioEngine::IsPlaying(int ChannelId) const
{
	auto FoundIt = Channels.find(ChannelId);
	if (FoundIt == Channels.end())
	{
		return false;
	}

	bool playing = false;
	//ErrorCheck(FoundIt->second->isPlaying(&playing));
	return playing;
}


/**************************************************************************/
/*!
\brief
Pauses the given channel.

\param int
The channel to be paused.

\return
void
*/
/**************************************************************************/
void AudioEngine::PauseChannel(const int ChannelId)
{
	auto FoundIt = Channels.find(ChannelId);
	if (FoundIt == Channels.end())
	{
		return;
	}

	ErrorCheck(FoundIt->second->setPaused(true));
}


/**************************************************************************/
/*!
\brief
Pauses the given Event.

\param string
The name of the event that we want to be paused.

\return
void
*/
/**************************************************************************/
void AudioEngine::PauseEvent(const string& EventName)
{
	auto FoundIt = Events.find(EventName);
	if (FoundIt == Events.end())
	{
		return;
	}

	ErrorCheck(FoundIt->second->setPaused(true));
}

/**************************************************************************/
/*!
\brief
Changes all of the sounds in the system to being paused.

\return
void
*/
/**************************************************************************/
void AudioEngine::PauseEngine()
{
	auto SoundsIt = Channels.begin();
	while (SoundsIt != Channels.end())
	{
		ErrorCheck(SoundsIt->second->setPaused(true));
		++SoundsIt;
	}

	auto EventIt = Events.begin();
	while (EventIt != Events.end())
	{
		ErrorCheck(EventIt->second->setPaused(true));
		++EventIt;
	}
}


/**************************************************************************/
/*!
\brief
Changes all of the sounds in the system to not being paused.

\return
void
*/
/**************************************************************************/
void AudioEngine::UnPauseEngine()
{
	auto SoundsIt = Channels.begin();
	while (SoundsIt != Channels.end())
	{
		ErrorCheck(SoundsIt->second->setPaused(false));
		++SoundsIt;
	}

	auto EventIt = Events.begin();
	while (EventIt != Events.end())
	{
		ErrorCheck(EventIt->second->setPaused(false));
		++EventIt;
	}
}


/**************************************************************************/
/*!
\brief
Checks if the given channel is paused.

\param int
The channel to be checked.

\return
Whether the channel is paused or not.
*/
/**************************************************************************/
bool AudioEngine::IsChannelPaused(const int ChannelId)
{
	auto FoundIt = Channels.find(ChannelId);
	if (FoundIt == Channels.end())
	{
		//Change to throw errors
		return false;
	}
	bool* paused = nullptr;
	ErrorCheck(FoundIt->second->getPaused(paused));
	return *paused;
}


/**************************************************************************/
/*!
\brief
Checks if the given event is paused.

\param string
The name of the event that's going being checked.

\return
Whether the event is paused or not.
*/
/**************************************************************************/
bool AudioEngine::IsEventPaused(const string& EventName)
{
	auto FoundIt = Events.find(EventName);
	if (FoundIt == Events.end())
	{
		//Change to throw errors

		return false;
	}
	bool* paused = nullptr;
	ErrorCheck(FoundIt->second->getPaused(paused));
	return *paused;
}


