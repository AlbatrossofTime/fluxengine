/******************************************************************************/
/*!
\file   Sound_Object.cpp
\author Deni Van Winkle
\par    email: deni.vanwinkle\@digipen.edu
\par    DigiPen login: deni.vanwinkle
\par    Course: GAM200
\par    Project: Shisan

Copyright � 2017 DigiPen (USA) Corporation.
*/
/******************************************************************************/
#include "Audio_Engine.h"
#include "Sound_Object.h"
#include "Engine.h"
#include "System.h"



// Connection to Engine Ptr in Engine.cpp
extern FluxEngine* ENGINE;


// quick solution to test different things
extern AudioEngine* Audio;

/**************************************************************************/
/*!
\brief
Creates a new sound object with the necicarry paramiters for a simple sound

\param string
Name of the sound attached to this object.

\param Vec2
The position this object will start at.

\param float
The maximum volume the object can be at from 0.0f to 1.0f.
*/
/**************************************************************************/
SoundObject::SoundObject(string sound_name, Vec2<float, float> pos, float Maxvolume)
{
	studio = false;
	Sound_Name = sound_name;
	Bank_Name = "None";
	Position = pos;
	MaxVolume = Maxvolume;
	ChannelId = 0;
	//audioEngine = ENGINE->GetSystem(Systems::Sys_Audio);
	audioEngine = Audio;
}


/**************************************************************************/
/*!
\brief
Creates a new sound object with the necicarry paramiters for a studio 
sound.

\param string
Name of the sound attached to this object.

\param string
Name of the bank that the sound is located in.

\param Vec2
The position this object will start at.

\param float
The maximum volume the object can be at from 0.0f to 1.0f.
*/
/**************************************************************************/
SoundObject::SoundObject(string sound_name, string bank_name, Vec2<float, float> pos, float volume)
{
	studio = true;
	Sound_Name = sound_name;
	Bank_Name = bank_name;
	Position = pos;
	MaxVolume = volume;
	ChannelId = 0;
	audioEngine = Audio;
}


/**************************************************************************/
/*!
\brief
Creates a new sound object with the necicarry paramiters from an already
exising object.

\param SoundObject
The original object that's going to be copied into the new one.
*/
/**************************************************************************/
SoundObject::SoundObject(SoundObject &original)
{
	studio = original.studio;
	Sound_Name = original.Sound_Name;
	Bank_Name = original.Bank_Name;
	Position = original.Position;
	MaxVolume = original.MaxVolume;
	audioEngine = original.audioEngine;
	ChannelId = 0;
}

/**************************************************************************/
/*!
\brief
Gets the position that the object is currently at.

\return
The current position of the object.
*/
/**************************************************************************/
Vec2<float, float> SoundObject::GetPosition()
{
	return Position;
}


/**************************************************************************/
/*!
\brief
Sets the current position of this object to the given value.

\param Vec2
The position for the object to be places at.

\return
Void
*/
/**************************************************************************/
void SoundObject::SetPosition(Vec2<float, float> pos)
{
	if (pos != Position)
	{
		Position = pos;
	}
}


/**************************************************************************/
/*!
\brief
Changes the max volume of this object based on the given value

\param float
The new value of the max volume from 0 to 1;

\return
void
*/
/**************************************************************************/
void SoundObject::SetMaxVolume(float max)
{
	if (max <= 1 && max >= 0)
	{
		MaxVolume = max;
	}
}


/**************************************************************************/
/*!
\brief
Checks for the audio engine then decides which update function to call.

\return
void
*/
/**************************************************************************/
void SoundObject::Update()
{
	if (audioEngine)
	{
		if (studio == true)
		{
			StudioUpdate();
		}
		else
		{
			SoundUpdate();
		}
	}
}


/**************************************************************************/
/*!
\brief
Updates the studio sound with it's given functions and scales the volume
according to the distance from the player.

\return
void
*/
/**************************************************************************/
void SoundObject::StudioUpdate()
{
	float DistToScreenCenter = 0.0f;
	if (DistToScreenCenter <= 51.0f)
	{
		if (!audioEngine->IsBankLoaded(Bank_Name))
		{
			audioEngine->LoadBank(Bank_Name, FMOD_STUDIO_LOAD_BANK_NORMAL);
		}

		if (audioEngine->IsEventPlaying(Sound_Name))
		{
			audioEngine->SetEventVolume(Sound_Name, VolumeByDistance());
		}
		else
		{
			audioEngine->PlayEvent(Sound_Name);
			audioEngine->SetEventVolume(Sound_Name, VolumeByDistance());
		}
	}
	else if (audioEngine->IsEventPlaying(Sound_Name))
	{

	}
}

/**************************************************************************/
/*!
\brief
Updates basic sounds, also scaling their volume according to the distance 
from the player

\return
void
*/
/**************************************************************************/
void SoundObject::SoundUpdate()
{

	//Need to add the distance to the center of the screen
	float DistToScreenCenter = 0.0f;
	if (DistToScreenCenter <= 51.0f)
	{
		if (!(ChannelId > 0))
		{
			audioEngine->LoadSound(Sound_Name);
			ChannelId = audioEngine->PlaySound_(Sound_Name, MaxVolume);
		}
		else if (audioEngine->IsPlaying(ChannelId))
		{
			audioEngine->SetChannelvolume(ChannelId, VolumeByDistance());
		}
		else
		{
			ChannelId = audioEngine->PlaySound_(Sound_Name, MaxVolume);
		}
	}
}


/**************************************************************************/
/*!
\brief
Stops the current sound playing.

\return
void
*/
/**************************************************************************/
void SoundObject::StopSound()
{
	if (audioEngine)
	{
		if (studio == true)
		{
			audioEngine->StopEvent(Sound_Name);
		}
		else
		{
			audioEngine->StopChannel(ChannelId);
		}
	}
}


/**************************************************************************/
/*!
\brief
Copies all of the values from the oringinal object into the current one.

\param SoundObject
The original object that's going to be copied into the new one.
*/
/**************************************************************************/
SoundObject& SoundObject::operator=(const SoundObject& original)
{
	studio = original.studio;
	Sound_Name = original.Sound_Name;
	Bank_Name = original.Bank_Name;
	Position = original.Position;
	MaxVolume = original.MaxVolume;
	ChannelId = 0;
	return *this;
}


/**************************************************************************/
/*!
\brief
Scales the volume of the object according to its max volume and it's
distance from the player. 

\return
The value between 0 and 1 that the volume should be currently set at.
*/
/**************************************************************************/
float SoundObject::VolumeByDistance()
{
	//Need screen width
	//currently scaling by an arbitary number
	float halfScreenDist = 400;

	Vec2<float, float> playerpos = ENGINE->playerObject->position;

	float Distance = Position.Distance(playerpos);
	float scale = (halfScreenDist - Distance) / halfScreenDist;

	scale *= MaxVolume;

	if (scale > 1)
	{
		return 1.0f;
	}
	else if (scale < 0)
	{
		return 0;
	}
	else
	{
		return scale;
	}
}