
/**************************************************************************/
/*!
\file   Graphics.cpp
\author 
\par    
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief

Functions include:

*/
/**************************************************************************/
#include "Graphics.h"
#include "ShaderLoader.h"
#include "Engine.h"
#include "Window.h"
#include <iostream>

extern FluxEngine* ENGINE;

/*const char *vertexShaderSource = "#version 330 core\n"
"layout (location = 0) in vec3 aPos;\n"
"void main()\n"
"{\n"
"   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
"}\0";
const char *fragmentShaderSource = "#version 330 core\n"
"out vec4 FragColor;\n"
"void main()\n"
"{\n"
"   FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
"}\n\0";*/

//float playerObjectVerts[] = { 
//	ENGINE->playerObject 
//}

unsigned int playerObjectIndices[] = {  // note that we start from 0!
	0, 1, 2,  // first Triangle
	1, 2, 3   // second Triangle
};

float vertices[] = {
	0.5f,  0.5f,  // top right
	0.5f, -0.5f,  // bottom right
	-0.5f, -0.5f,  // bottom left
	-0.5f,  0.5f  // top left 
};
unsigned int indices[] = {  // note that we start from 0!
	0, 1, 2,  // first Triangle
	1, 2, 3   // second Triangle
};
float texCoords[] = {
  1.0f, 1.0f,
  1.0f, 0.0f,
  0.0f, 0.0f,
  0.0f, 1.0f
};

/**
 * \brief 
 */
Graphics::Graphics() : System(std::string("Graphics System"), Systems::Sys_Graphics){
  render_prog = NULL;
}



/**
 * \brief 
 */
void Graphics::Init() {
  render_prog = LoadShader("VertexShader.glsl", "FragmentShader.glsl");
  glUseProgram(render_prog);

  int width  = 1780;
  int height = 1097;

  data = SOIL_load_image("lizboypng.png", &width, &height, 0, SOIL_LOAD_RGB);

  /* check for an error during the load process */
  if (0 == data)
  {
    printf("SOIL loading error: '%s'\n", SOIL_last_result());
  }
}

void PrintPlayer() {
  std::cout << "Player Position" << ENGINE->playerObject->position.x << " " << ENGINE->playerObject->position.y << std::endl;
  std::cout << "Player Vert1 " << ENGINE->playerObject->mesh[0].x << " " << ENGINE->playerObject->mesh[0].y << std::endl;
  std::cout << "Player Vert2 " << ENGINE->playerObject->mesh[1].x << " " << ENGINE->playerObject->mesh[1].y << std::endl;
  std::cout << "Player Vert3 " << ENGINE->playerObject->mesh[2].x << " " << ENGINE->playerObject->mesh[2].y << std::endl;
  std::cout << "Player Vert4 " << ENGINE->playerObject->mesh[3].x << " " << ENGINE->playerObject->mesh[3].y << std::endl;
}

void Graphics::Draw() {
  glGenVertexArrays(1, &VAO);

  glBindVertexArray(VAO);
  glGenBuffers(1, &VBO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(ENGINE->playerObject->openGLContainer[0]) * ENGINE->playerObject->openGLContainer.size(), &(ENGINE->playerObject->openGLContainer[0]), GL_STATIC_DRAW);
  
  glGenBuffers(1, &IBO);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
  //glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), &indices, GL_STATIC_DRAW);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(ENGINE->playerObject->indicesP), &(ENGINE->playerObject->indicesP[0]), GL_STATIC_DRAW);
  
  posAttrib = glGetAttribLocation(render_prog, "position");
  glEnableVertexAttribArray(posAttrib);
  glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);

  texAttrib = glGetAttribLocation(render_prog, "texcoord");
  glEnableVertexAttribArray(texAttrib);
  glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));

  glGenTextures(1, &texture);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1780, 1097, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
  glUniform1i(glGetUniformLocation(render_prog, "texcoord"), 0);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

/**
 * \brief 
 * \param dt 
 */
void Graphics::Update(float dt) {
  //PrintPlayer();
	Draw();
	glBindVertexArray(VAO);
  GLenum check = glGetError();
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	WindowPtr windowSystemPtr = GETSYS(Window);

	SDL_GL_SwapWindow(windowSystemPtr->win);
}

/**
 * \brief 
 */
void Graphics::ShutDown() {
  SOIL_free_image_data(data);
  glDeleteTextures(1, &texture);
  glDeleteShader(fragmentShader);
  glDeleteShader(vertexShader);
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &IBO);
}