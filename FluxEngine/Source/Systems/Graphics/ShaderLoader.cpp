#include "ShaderLoader.h"

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iterator>
/**************************************************************************/
/*!
\file   ShaderLoader.cpp
\author
\par
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief

Functions include:

*/
/**************************************************************************/

/**
 * \brief 
 * \param filepath 
 * \return 
 */
std::string readFile(const char* filepath)
{
	std::ifstream myFile(filepath);
	std::string content((std::istreambuf_iterator<char>(myFile)), std::istreambuf_iterator<char>());

	return content;
}

/**
 * \brief 
 * \param vertex_path 
 * \param fragment_path 
 * \return 
 */
GLuint LoadShader(const char *vertex_path, const char *fragment_path)
{
	// Create the Vertex Shader GameObject (VSO) and Fragment Shader GameObject (FSO).
	// These are needed the final result objects that we are going to bind into one program.
	// Most of the time "GLuint" are unique identifiers so OpenGL knows what is what.
	GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);

	// Take the shader files, convert them to strings, and make c-strings out of them.
	std::string vertShaderString = readFile(vertex_path);
	std::string fragShaderString = readFile(fragment_path);
	const char *vertShaderSource = vertShaderString.c_str();
	const char *fragShaderSource = fragShaderString.c_str();

	// Set the source code into the VSO
	glShaderSource(vertShader, 1, &vertShaderSource, NULL);
	glShaderSource(fragShader, 1, &fragShaderSource, NULL);
	
	// Compile, check, and log the process if in error - VSO
	GLint isCompiled = 0;

	glCompileShader(vertShader);
	glGetShaderiv(vertShader, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint logLength = 0;
		glGetShaderiv(vertShader, GL_INFO_LOG_LENGTH, &logLength);
		std::vector<char> vertShaderError((logLength > 1) ? logLength : 1);
		glGetShaderInfoLog(vertShader, logLength, NULL, &vertShaderError[0]);
		std::cout << &vertShaderError[0] << std::endl;

		glDeleteShader(vertShader);
		return 0;
	}

	// Compile, check, and log the process if in error - FSO
	isCompiled = 0;

	glCompileShader(fragShader);
	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint logLength = 0;
		glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &logLength);
		std::vector<char> fragShaderError((logLength > 1) ? logLength : 1);
		glGetShaderInfoLog(fragShader, logLength, NULL, &fragShaderError[0]);
		std::cout << &fragShaderError[0] << std::endl;

		glDeleteShader(vertShader);
		glDeleteShader(fragShader);
		return 0;
	}

	// Now that the objects have been created,
	// we can actually go about creating the program object,
	GLuint program = glCreateProgram();
	// attaching the shaders to the program,
	glAttachShader(program, vertShader);
	glAttachShader(program, fragShader);
	// and linking the program.
	glLinkProgram(program);

	// One final pass of checks to see if the program was linked correctly.
	// If it was, then it will return the GLuint ID of the program.
	GLint isLinked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint logLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
		std::vector<GLchar> infoLog(logLength);
		glGetProgramInfoLog(program, logLength, &logLength, &infoLog[0]);

		// Get rid of the trash
		glDeleteProgram(program);
		glDeleteShader(vertShader);
		glDeleteShader(fragShader);
		return 0;
	}
	
	// Deletion of shaders is deferred until detached, so do it here.
	glDetachShader(program, vertShader);
	glDetachShader(program, fragShader);

	return program;
}

/**
 * \brief 
 * \param compute_path 
 * \return 
 */
GLuint LoadComputeShader(const char *compute_path)
{
	GLuint compShader = glCreateShader(GL_COMPUTE_SHADER);
	std::string compShaderString = readFile(compute_path);
	const char *compShaderSource = compShaderString.c_str();
	glShaderSource(compShader, 1, &compShaderSource, NULL);
	// error checking
	GLint isCompiled = 0;
	glCompileShader(compShader);
	glGetShaderiv(compShader, GL_COMPILE_STATUS, &isCompiled);
	if (isCompiled == GL_FALSE)
	{
		GLint logLength = 0;
		glGetShaderiv(compShader, GL_INFO_LOG_LENGTH, &logLength);
		std::vector<char> compShaderError((logLength > 1) ? logLength : 1);
		glGetShaderInfoLog(compShader, logLength, NULL, &compShaderError[0]);
		std::cout << &compShaderError[0] << std::endl;
		glDeleteShader(compShader);
		return 0;
	}
	// error checking done

	// Now that the objects have been created,
	// we can actually go about creating the program object,
	GLuint program = glCreateProgram();
	// attaching the shaders to the program,
	glAttachShader(program, compShader);
	// and linking the program.
	glLinkProgram(program);

	// One final pass of checks to see if the program was linked correctly.
	// If it was, then it will return the GLuint ID of the program.
	GLint isLinked = 0;
	glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);
	if (isLinked == GL_FALSE)
	{
		GLint logLength = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
		std::vector<GLchar> infoLog(logLength);
		glGetProgramInfoLog(program, logLength, &logLength, &infoLog[0]);

		// Get rid of the trash
		glDeleteProgram(program);
		glDeleteShader(compShader);
		return 0;
	}

	// Deletion of shaders is deferred until detached, so do it here.
	glDetachShader(program, compShader);

	return program;
}