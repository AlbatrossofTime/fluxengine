//#include "stdafx.h"
#include "Window.h"
#include "Engine.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
/**************************************************************************/
/*!
\file   Window.cpp
\author
\par
\par    DigiPen login: j.ibrahim
\par    Course: GAME 200
\par   Copyright � 2014 DigiPen (USA) Corp. and its owners. All Rights Reserved.
\date   9/28/2017
\brief

Functions include:

*/
/**************************************************************************/

// Connection to Engine Ptr in Engine.cpp
extern FluxEngine* ENGINE;

/**
 * \brief 
 */

Window::Window() : Systems::System(std::string("Window System"), Systems::Sys_Window)
{
  title = "FluxEngine V.01";
  windowWidth = 800;
  windowHeight = 640;
  win = NULL;
}

/**
 * \brief 
 */
void Window::Init()
{
  // Sets OpenGL version.
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

  // "Turn on" SDL
  if (SDL_Init(SDL_INIT_VIDEO) != 0)
  {
    std::cout << "Error Initializing SDL: " << SDL_GetError() << std::endl;
  }

  // Creates the window.
  win = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                         windowWidth, windowHeight, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
  if (win == nullptr)
  {
    std::cout << "Something went wrong with CreateWindow: " << SDL_GetError() << std::endl;
    //SDL_Quit();
  }

  // Creates the OpenGL Context.
  GLcontext = SDL_GL_CreateContext(win);
  if (GLcontext == nullptr)
  {
    std::cout << "Something went wrong with CreateContext: " << SDL_GetError() << std::endl;
    //SDL_Quit();
  }

  // "Turn on" OpenGL
  glewExperimental = GL_TRUE;
  glewInit();
}

/**
 * \brief 
 * \param dt 
 */
void Window::Update(float dt)
{
  PollEvents();
  // SLR TODO : I would rather have the following in the graphics system.
  // R G B A
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);
  //SDL_GL_SwapWindow(win);
}

/**
 * \brief 
 */
void Window::ShutDown()
{
  SDL_GL_DeleteContext(GLcontext);
}

/**
 * \brief 
 */
void Window::PollEvents()
{
  theMouse.LTrigger = theMouse.RTrigger = false;
  while (SDL_PollEvent(&events))
  {
    PollWindowEvent(events);
    PollKeyEvent(events);
    PollMouseEvent(events);
  }
}

/**
 * \brief 
 * \param currentEvent 
 */
void Window::PollWindowEvent(SDL_Event& currentEvent)
{
  switch (currentEvent.type)
  {
  case SDL_WINDOWEVENT:
    switch (currentEvent.window.event)
    {
    case SDL_WINDOWEVENT_SIZE_CHANGED:
      SDL_GetWindowSize(win, &windowWidth, &windowHeight);
      // Calculates correct aspect ratio for OpenGL.
      glViewport(0, 0, windowWidth,
                 static_cast<GLsizei>(windowWidth / (static_cast<float>(windowWidth) / windowHeight)));
      break;
    case SDL_WINDOWEVENT_CLOSE:
      break;
    case SDL_WINDOWEVENT_MOVED:
      break;
    case SDL_WINDOWEVENT_FOCUS_GAINED:
      break;
    case SDL_WINDOWEVENT_FOCUS_LOST:
      break;
    }
    break;
  case SDL_QUIT:
    // For regular Windows API "close window" functionality.
    ENGINE->Stop();
    break;
  default:
    break;
  }
}

// SLR TODO: Input functionality below.
void Window::PollKeyEvent(SDL_Event & currentEvent)
{
	switch (currentEvent.type)
	{
	case SDL_KEYDOWN:
		if (currentEvent.key.keysym.sym == SDLK_ESCAPE)
			std::cout << "ESCAPE PRESSED" << std::endl;
		if (currentEvent.key.keysym.sym == SDLK_LEFT) {
			  ENGINE->playerObject->position.x -= .2f;
			  ENGINE->calcMesh();
			  //std::cout << "LEFT PRESSED" << std::endl;
		}
			if (currentEvent.key.keysym.sym == SDLK_RIGHT) {
			  ENGINE->playerObject->position.x += .2f;
			  ENGINE->calcMesh();
			  //std::cout << "RIGHT PRESSED" << std::endl;
		}
			if (currentEvent.key.keysym.sym == SDLK_DOWN) {
			  ENGINE->playerObject->position.y -= .2f;
			  ENGINE->calcMesh();
			  //std::cout << "DOWN PRESSED" << std::endl;
		}
			if (currentEvent.key.keysym.sym == SDLK_UP) {
			  ENGINE->playerObject->position.y += .2f;
			  ENGINE->calcMesh();
			  //std::cout << "UP PRESSED" << std::endl;
		}
		if (currentEvent.key.keysym.sym == SDLK_SPACE)
			std::cout << "SPACEBAR PRESSED" << std::endl;
		break;
	case SDL_KEYUP:
		break;
	}
}

// SLR TODO: Consolidate up this mess.
void Window::PollMouseEvent(SDL_Event & currentEvent)
{
	switch (currentEvent.type)
	{
		case SDL_MOUSEMOTION:
		{
			// SLR TODO : IMPLEMENT UPDATING MOUSE POSITION 
		}
		break;
		case SDL_MOUSEBUTTONDOWN:
			if (currentEvent.button.button == SDL_BUTTON_LEFT)
			{
				std::cout << "LMB PRESSED" << std::endl;
				if (theMouse.LPressed == false)
				{
					theMouse.LTrigger = true;
				}
				theMouse.LReleased = false;
				theMouse.LPressed = true;
			}
			if (currentEvent.button.button == SDL_BUTTON_RIGHT)
			{
				std::cout << "RMB PRESSED" << std::endl;
				if (theMouse.RPressed == false)
				{
					theMouse.RTrigger = true;
				}
				theMouse.RReleased = false;
				theMouse.RPressed = true;
			}
			break;
		case SDL_MOUSEBUTTONUP:
			if (currentEvent.button.button == SDL_BUTTON_LEFT)
			{
				//std::cout << "LMB UP" << std::endl;
				if (theMouse.LPressed == true)
				{
					theMouse.LReleased = true;
				}
				else
					theMouse.LReleased = false;

				theMouse.LPressed = false;
				theMouse.LTrigger = false;
			}
			if (currentEvent.button.button == SDL_BUTTON_RIGHT)
			{
				//std::cout << "RMB UP" << std::endl;
				if (theMouse.RPressed == true)
				{
					theMouse.RReleased = true;
				}
				else
					theMouse.RReleased = false;

				theMouse.RPressed = false;
				theMouse.RTrigger = false;
			}
			break;

		default:
			break;
	}
}
